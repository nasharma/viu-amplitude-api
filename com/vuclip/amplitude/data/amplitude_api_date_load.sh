#!/bin/sh

source_path="/opt/app/insights-viu/viu_etl/src/amplitudeApi.py"

configuration_path="/opt/app/insights-viu/viu_etl/config/"

amplitude_config=$configuration_path"amplitude_api.config"

cloud_config=$configuration_path"amplitude_api_cloud.config"

global_config=$configuration_path"amplitude_api_global.config"

env="dev"

current_date=$(date +"%Y%m%d")

start_date=$(date -d "-5 hours" +"%Y%m%dT%H")

end_date=$(date -d "-1 hours" +"%Y%m%dT%H")

log_location="/opt/app/logs/"

log_file=$log_location"amplitude_api_log_"$start_date

python $source_path -a $amplitude_config -c $cloud_config -g $global_config -p $env -s $start_date -e $end_date >> $log_file
