import uuid
import datetime
import time
import logging
import psycopg2
import smtplib

from google import auth
from google.cloud import bigquery
from collections import defaultdict



def wait_for_job(job):
    while True:
        job.reload()  # Refreshes the state via a GET request.
        if job.state == 'DONE':
            if job.error_result:
                raise RuntimeError(job.errors)
            return
        logging.debug("job still running... sleeping for 10 seconds")
        time.sleep(10)


def get_redshift_audit_query(redshift_app_table_sets ,bq_app_names, start_time ,end_time):
    query = " "
    for app, values in redshift_app_table_sets.iteritems():
        for table in values:
           table1 = "{}.{}".format(app,table)
           if(table1 in redshift_avail_tables):
                query += " select current_date as check_date, cast(server_upload_time as date) as load_date, 'redshift' as source, '{}_{}' as table_name, count(*) as row_count " \
                      "from {}.{} where  server_upload_time >= '{}' AND server_upload_time < '{}' group by 1, 2, 3, 4  UNION ALL ".format(bq_app_names.get(app[3:]),table,app,table,start_time,end_time)
        #break;
    query = query[:-10]
    return query

def get_bigquery_audit_query(bq_app_table_sets, start_time ,end_time):
    query = " "
    dataset ="amplitude"
    for app, values in bq_app_table_sets.iteritems():
        for table in values:
            query += " select current_date as check_date, date(_partitiontime) as load_date , 'bigquery' as source, '{}' as table_name, count(*) as row_count " \
                      "from {}.{} where  server_upload_time >= '{}' AND server_upload_time < '{}' AND _PARTITIONTIME >= '{}' and _PARTITIONTIME < '{}' group by 1, 2, 3, 4 UNION ALL ".format(table,dataset,table,start_time,end_time,start_time,end_time)
        #break;
    query = query[:-10]
    return query

def get_bq_compare_table_list():
    credentials, project = auth.default()
    bigquery_client = bigquery.Client("vuclipdataflow-1301", credentials)
    dataset = bigquery_client.dataset("amplitude")
    table_list = dataset.list_tables()
    bq_app_names = dict()
    redshift_table_str=""
    bq_app_table_sets = defaultdict(set)
    redshift_app_table_sets = defaultdict(set)
    for table in table_list:
        index = table.name.index("1")
        bq_app_name = table.name[:(index + 6)]
        bq_app_names[table.name[index:(index + 6)]]=bq_app_name
        redshift_app_name = "app"+table.name[index:(index + 6)]
        redshift_table = table.name[(index + 7):]
        redshift_table_str += "'" + redshift_table + "' , "
        bq_app_table_sets[bq_app_name].add(table.name.encode('utf-8'))
        redshift_app_table_sets[redshift_app_name].add(redshift_table.encode('utf-8'))

    redshift_table_str = redshift_table_str[:-2]
    return bq_app_table_sets,redshift_app_table_sets,bq_app_names,redshift_table_str


def get_redshift_avail_tables(redshift_table_str):
    con = psycopg2.connect(dbname='vuclip', host='vuclip.redshift.amplitude.com', port='5439', user='vuclip',
                           password='w6c4Si8QQHsugczOIxgmSh6F')
    query = " select table_schema , table_name from information_schema.tables where table_name in ({})".format(
        redshift_table_str)
    print query
    cur = con.cursor()
    cur.execute(query)
    data = cur.fetchall()
    redshift_avail_tables = list()
    for row in data:
        table = "{}.{}".format(row[0], row[1])
        redshift_avail_tables.append(table)
    cur.close()
    con.close()

    return redshift_avail_tables


def send_mail(result_file_name):
    logging.debug('sending email...')
    SERVER = "localhost"
    FROM = "notifications@viu-etl-1.vuclipdataflow-1301.com"
    SUBJECT = "Amplitude check for date: {}".format(datetime.date.today().strftime('%Y-%m-%d'))
    TO = ['koushik.kulkarni@vuclip.com']
    with open (result_file_name, 'r') as resultfile:
        TEXT = resultfile.read()
    message = """\
From: %s
To: %s
Subject: %s

%s
""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
    server = smtplib.SMTP(SERVER)
    server.set_debuglevel(3)
    server.sendmail(FROM, TO, message)
    server.quit()
    logging.debug('email sent!')

#logging.basicConfig(level=logging.INFO, filename="/opt/app/viu_etl/logs/amplitude_audit.log", filemode="a+", format="%(asctime)-15s %(levelname)-8s %(message)s")
logging.basicConfig(level=logging.INFO, filename="output/amplitude_audit.log", filemode="a+", format="%(asctime)-15s %(levelname)-8s %(message)s")

logging.info("starting Amplitude - bigquery check for {}...".format(datetime.date.today().strftime('%Y%m%d')))

#with open ('/opt/app/viu_etl/src/bigquery_audit_query.sql', 'r') as sqlfile:
#    bigquery_audit_query = sqlfile.read().replace('\n', ' ')

bq_app_table_sets, redshift_app_table_sets,bq_app_names,redshift_table_str = get_bq_compare_table_list()
redshift_avail_tables = get_redshift_avail_tables(redshift_table_str)

start_time = datetime.datetime.now().strftime("%Y-%m-%d") + " 00:00:00"
end_time = datetime.datetime.now().strftime("%Y-%m-%d") + " 12:00:00"

start_time='2017-06-12 00:00:00'
end_time='2017-06-13 00:00:00'
partition_time='20130612'

#partition_time=datetime.date.today().strftime('%Y%m%d'))
print ("start time : {} , end time {}  ".format(start_time, end_time))

bigquery_audit_query = get_bigquery_audit_query(bq_app_table_sets,start_time ,end_time)

logging.info("bigquery audit query: {}".format(bigquery_audit_query))
credentials, project = auth.default()
bigquery_client = bigquery.Client("vuclipdataflow-1301", credentials)
viu_dataset = bigquery_client.dataset('viu_etl_test')
amplitude_audit_table = viu_dataset.table('amplitude_audit${}'.format(partition_time))

logging.info("starting bigquery rowcount job...")
bigquery_audit_job_name = "Bigquery_audit_job_{}_{}".format(datetime.date.today().strftime('%Y%m%d'), str(uuid.uuid4()))
bigquery_audit_job = bigquery_client.run_async_query(bigquery_audit_job_name, bigquery_audit_query)
bigquery_audit_job.use_legacy_sql = False
bigquery_audit_job.allow_large_results = True
bigquery_audit_job.flatten_results = False
bigquery_audit_job.destination = amplitude_audit_table
bigquery_audit_job.write_disposition = 'WRITE_TRUNCATE'
bigquery_audit_job.begin()
wait_for_job(bigquery_audit_job)
logging.info("bigquery rowcount job completed!")

logging.info("starting Amplitude - redshift check for {}...".format(datetime.date.today().strftime('%Y%m%d')))


#with open ('/opt/app/viu_etl/src/redshift_audit_query.sql', 'r') as sqlfile:
#    redshift_audit_query = sqlfile.read().replace('\n', ' ')

redshift_audit_query = get_redshift_audit_query(redshift_app_table_sets, bq_app_names,start_time ,end_time)
logging.info("redshift audit query: {}".format(redshift_audit_query))

con=psycopg2.connect(dbname= 'vuclip', host='vuclip.redshift.amplitude.com', port= '5439', user= 'vuclip', password= 'w6c4Si8QQHsugczOIxgmSh6F')
cur = con.cursor()
cur.execute(redshift_audit_query)
data = cur.fetchall()
#redshift_file = open ('/opt/app/viu_etl/data/redshift_output.txt', 'w')
redshift_file = open ('output/redshift_output.txt', 'w')

for row in data:
    redshift_file.write(row[0].strftime('%Y-%m-%d') + "," + row[1].strftime('%Y-%m-%d') + "," + row[2] + "," + row[3] + "," + str(row[4]) + "\n")

redshift_file.close()
cur.close()
con.close()
logging.info("redshift row counts fetched!")

#redshift_file = open ('/opt/app/viu_etl/data/redshift_output.txt', 'rb')
redshift_file = open ('output/redshift_output.txt', 'rb')
amplitude_audit_table.reload()
redshift_audit_job = amplitude_audit_table.upload_from_file(redshift_file, source_format='text/csv', write_disposition='WRITE_APPEND')
logging.info("readshift data load job started!")
wait_for_job(redshift_audit_job)
logging.info("redshift data load job completed!")

logging.info("Starting comparison job...")
compare_query = """select * from (
select rs.load_date, rs.table_name, rs.check_date rs_check_date, rs.row_count rs_row_count, bq.check_date bq_check_date, bq.row_count bq_row_count, 
case when coalesce (bq.row_count, 0) <> coalesce (rs.row_count, 0) then 'ERROR' else 'MATCH' end comparison
from (select *
  from (
    select *, row_number() over (partition by table_name, load_date order by check_date desc) as row_num
    from `viu_etl_test.amplitude_audit`
    where source = 'redshift'
    and load_date between date_sub(current_date, interval 7 DAY) and date_sub(current_date, interval 1 DAY)
  ) a 
  where row_num = 1   
)rs
left join (select *
  FROM (
    select *, row_number() over (partition by table_name, load_date order by check_date desc) as row_num
    from `viu_etl_test.amplitude_audit`
    where source = 'bigquery'
    and load_date between date_sub(current_date, interval 7 DAY) and date_sub(current_date, interval 1 DAY)
  ) a 
  where row_num = 1    
) bq
on bq.load_date = rs.load_date
and bq.table_name = rs.table_name ) err
where  comparison = 'ERROR' 
order by 1 desc, 2 ;"""

audit_job_name = "Audit_job_{}_{}".format(datetime.date.today().strftime('%Y%m%d'), str(uuid.uuid4()))
audit_job = bigquery_client.run_async_query(audit_job_name, compare_query)
audit_job.use_legacy_sql = False
audit_job.begin()
logging.info("comparison job started!")
wait_for_job(audit_job)

logging.info("Compare job finished. Fetching results...")
#result_file_name = '/opt/app/viu_etl/data/result.txt'
result_file_name = 'output/result.txt'
result_file = open (result_file_name, 'w')

result_file.write("load_date,table_name,rs_check_date,rs_row_count,bq_check_date,bq_row_count,comparison\n")

query_results = audit_job.results()
page_token = None

while True:
    rows, total_rows, page_token = query_results.fetch_data( max_results=10, page_token=page_token)
    for row in rows:
      result_file.write(",".join((str(row[0]), row[1], str(row[2] or ''), str(row[3] or ''), str(row[4] or ''), str(row[5] or ''), row[6] )) + '\n')
    if not page_token:
      break

result_file.close()
logging.info("Comparison results stored in {}.".format(result_file_name))

#send_mail(result_file_name)