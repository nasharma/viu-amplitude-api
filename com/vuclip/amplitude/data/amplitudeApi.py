'''
Created on 10th April 2017

@author: Nikhil Sharma
'''

import getopt
import os
import re
import traceback
import shutil
import sys
import zipfile
from os import walk

import requests
import gzip
import ConfigParser
import urllib
import collections
import json
import logging
from google.cloud import bigquery
from google import auth
import time
from datetime import datetime
from google.cloud import storage
import concurrent.futures
import uuid


class AmplitudeApiData(object):
    tmp_folder_location = ""
    extracted_file_location = ""
    flatten_data_file_location = ""
    source_file_location = ""
    google_bigquery_data_set = ""
    data_file_location = ""
    pubsub_topic_name = ""
    bigquery_table_partition = ""
    google_storage_bucket_name = ""
    google_bigquery_project_name = ""
    processed_location = ""
    timestamp_keys = ['client_event_time', 'event_time', 'load_time', 'processed_time', 'user_creation_time',
                      'server_upload_time', 'client_upload_time']

    def __init__(self):
        print 'Initialized'
        self.load_time = str(datetime.now())
        self.amplitude_api_url = ''
        self.amplitude_api_user_name = ''
        self.amplitude_api_user_password = ''
        self.amplitude_api_platform = ''
        self.platform_name_project_id = ''
        self.start_date = ''
        self.end_date = ''
        self.all_logs_on_event_type = {}
        self.all_event_type_bigquery_schema = {}
        self.event_type_schema_fields = {}
        self.all_event_types = []
        self.GOOGLE_STORAGE = 'gs'
        credentials, project = auth.default()
        self.bigquery_client = bigquery.Client(self.google_bigquery_project_name, credentials)
        self.data_set = self.bigquery_client.dataset(self.google_bigquery_data_set)
        self.storage_client = storage.Client(self.google_bigquery_project_name, credentials)

        if __name__ == "__main__":
            self.main(sys.argv[1:])

    def main(self, argv):
        amplitude_config = ''
        cloud_config = ''
        global_config = ''
        env = ''
        start_date = ''
        end_date = ''
        try:
            opts, args = getopt.getopt(argv, "h:a:c:g:p:s:e:",
                                       ["amplitude.config=", "cloud.config=", "global.config=", "env=",
                                        "start_date=", "end_date="])
        except getopt.GetoptError:
            print 'amplitudeApi.py -a <amplitude.config> -c <cloud.config> -g <global.config> -p <env> ' \
                  '-s <start_date in yyyymmddTHH format> -e <end_date in yyyymmddTHH format>'
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print 'amplitudeApi.py -a <amplitude.config> -c <cloud.config> -g <global.config> -p <env> ' \
                      '-s <start_date in yyyymmddTHH format> -e <end_date in yyyymmddTHH format>'
                sys.exit()
            elif opt in ("-a", "--amplitude"):
                amplitude_config = arg
            elif opt in ("-c", "--cloud"):
                cloud_config = arg
            elif opt in ("-g", "--global.config"):
                global_config = arg
            elif opt in ("-p", "--env"):
                env = arg
            elif opt in ("-s", "--start_date"):
                start_date = arg
            elif opt in ("-e", "--end_date"):
                end_date = arg

        self.populate_amplitude_api_config(env, amplitude_config)
        self.populate_cloud_config(env, cloud_config)
        self.populate_global_config(env, global_config)
        self.start_date = start_date
        self.end_date = end_date
        self.prepare_amplitude_url()
        run_date = datetime.strptime(self.start_date, "%Y%m%dT%H")
        self.bigquery_table_partition = '${}{}{}'.format(run_date.year, "%02d" % run_date.month, "%02d" % run_date.day)

    def prepare_amplitude_url(self):
        url = self.amplitude_api_url
        parameters = {'start': self.start_date, 'end': self.end_date}
        self.amplitude_api_url = self.amplitude_api_url + "?" + urllib.urlencode(parameters)

    @staticmethod
    def read_config_file(file_name):
        config = ConfigParser.ConfigParser()
        config.read(file_name)
        return config

    def populate_amplitude_api_config(self, env, config_file):

        config = AmplitudeApiData.read_config_file(config_file)
        self.amplitude_api_url = config.get(env, 'amplitude_api_url')
        self.amplitude_api_user_name = config.get(env, 'amplitude_api_user_name')
        self.amplitude_api_user_password = config.get(env, 'amplitude_api_password')
        self.amplitude_api_platform = config.get(env, 'amplitude_api_platform')
        self.amplitude_api_project_id = config.get(env, 'amplitude_api_project_id')
        self.platform_name_project_id = '{}_{}'.format(self.amplitude_api_platform, self.amplitude_api_project_id)

    def populate_cloud_config(self, env, config_file):

        config = AmplitudeApiData.read_config_file(config_file)
        self.google_bigquery_data_set = config.get(env, 'bigquery_data_set')
        self.google_storage_bucket_name = config.get(env, 'cloud_storage_bucket')
        self.google_bigquery_project_name = config.get(env, 'bigquery_project')

    def populate_global_config(self, env, config_file):

        config = AmplitudeApiData.read_config_file(config_file)
        self.tmp_folder_location = config.get(env, 'local_tmp_location')
        self.data_file_location = '{}{}{}_{}{}'.format(self.tmp_folder_location, os.path.sep,
                                                       self.platform_name_project_id, "data", os.path.sep)
        AmplitudeApiData.create_directory_if_not_exists(self.data_file_location)
        self.extracted_file_location = '{}{}{}_{}{}'.format(self.tmp_folder_location, os.path.sep,
                                                            self.platform_name_project_id, "extracted", os.path.sep)
        AmplitudeApiData.create_directory_if_not_exists(self.extracted_file_location)
        self.source_file_location = '{}{}{}_{}{}'.format(self.tmp_folder_location, os.path.sep,
                                                         self.platform_name_project_id, "source_files", os.path.sep)
        AmplitudeApiData.create_directory_if_not_exists(self.source_file_location)
        self.flatten_data_file_location = '{}{}{}_{}{}'.format(self.tmp_folder_location, os.path.sep,
                                                               self.platform_name_project_id, "flatten_source_files",
                                                               os.path.sep)
        AmplitudeApiData.create_directory_if_not_exists(self.flatten_data_file_location)
        self.processed_location = '{}{}{}_{}{}'.format(self.tmp_folder_location, os.path.sep,
                                                       self.platform_name_project_id, "processed", os.path.sep)
        AmplitudeApiData.create_directory_if_not_exists(self.processed_location)

    @staticmethod
    def create_directory_if_not_exists(directory_name):
        if not os.path.exists(directory_name):
            os.makedirs(directory_name)

    def get_data_from_api(self):
        print('Calling API: {}'.format(self.amplitude_api_url))
        amplitude_api_request = requests.get(self.amplitude_api_url,
                                             auth=(self.amplitude_api_user_name, self.amplitude_api_user_password),
                                             stream=True)
        print(self.amplitude_api_url)
        if amplitude_api_request.status_code == 200:
            data_file_name = os.path.join(self.data_file_location,
                                          '{}_{}_{}_{}'.format(self.platform_name_project_id, self.start_date,
                                                               self.end_date, "data_file"))
            with open(data_file_name, "wb") as dataFile:
                amplitude_api_request.raw.decode_content = True
                shutil.copyfileobj(amplitude_api_request.raw, dataFile)
        print('Api call complete! status code: {} data writted to file: {}'.format(amplitude_api_request.status_code,
                                                                                   dataFile))
        return data_file_name

    @staticmethod
    def extract_data_file(source, destination):
        print('Starting extract_data_file: {}'.format(source))
        zip_ref = zipfile.ZipFile(source, 'r')
        zip_ref.extractall(destination)
        zip_ref.close()
        os.remove(source)
        print('Done extract_data_file to: {}'.format(destination))

    @staticmethod
    def unzip_gz_file(source, destination):

        with gzip.open(source, 'rb') as source_json_file, open(destination, 'wb') as destination_json_file:
            shutil.copyfileobj(source_json_file, destination_json_file)

    def extract_gz_file_from_extracted_folder(self):
        print('Starting extract_gz_file_from_extracted_folder')
        for (directory_path, directory_names, file_names) in walk(self.extracted_file_location):
            for sub_directory in directory_names:
                for (sub_directory_path, sub_directory_names, sub_file_names) in walk(
                        os.path.join(directory_path, sub_directory)):
                    files = []
                    files.extend(sub_file_names)
                    for data_file in files:
                        self.unzip_gz_file(os.path.join(sub_directory_path, data_file),
                                           os.path.join(self.source_file_location, data_file[:-2]))

                    shutil.rmtree(os.path.join(self.extracted_file_location, sub_directory_path))

        print('Done extract_gz_file_from_extracted_folder')

    def flatten_json_with_parent(self, d, parent_key='', sep='_'):
        items = []
        for k, v in d.items():
            new_key = parent_key.split("_", 1)[0] + sep + k if parent_key else k
            if isinstance(v, collections.MutableMapping):
                items.extend(self.flatten_json_with_parent(v, new_key, sep=sep).items())
            else:
                key = AmplitudeApiData.replace_chars_in_field_name(new_key)
                items.append((key, str(v)))
        return dict(items)

    @staticmethod
    def replace_chars_in_field_name(new_key):

        new_key = new_key.replace(' ', '_')
        new_key = re.sub(r'\W+', '', new_key.encode("ascii", "ignore"))
        new_key = re.sub(r'[!#$%&\'*+-.^`|~:]+', '', new_key)

        return new_key

    def flatten_source_files(self):
        print('Staring flatten_source_files')
        process_one_file = False
        for (directory_path, directory_names, file_names) in walk(self.source_file_location):
            for file_name in file_names:
                self.all_logs_on_event_type = {}
                with open(os.path.join(directory_path, file_name), 'r') as source_file:
                    print('Starting flattening for file: {} {}'.format(source_file.name, datetime.now()))
                    for log in source_file:
                        flat_record = self.flatten_json_with_parent(json.loads(log))
                        flat_record["load_time"] = self.load_time
                        self.populate_all_logs_dict(flat_record)
                        self.populate_event_type_table_schema(flat_record)
                    print('Done flattening for file: {} {}'.format(source_file.name, datetime.now()))
                source_file.close()
                self.write_log_types_to_indivitual_files(file_name)
                os.remove(source_file.name)
            if process_one_file:
                break
        print('Done flatten_source_files')

    def populate_event_type_table_schema(self, log):
        event_type = self.relace_chars_in_event_type(log["event_type"])

        if event_type not in self.all_event_types:
            self.all_event_types.append(event_type)

        if event_type in self.all_event_type_bigquery_schema:
            table_schema = self.all_event_type_bigquery_schema[event_type]
            for key in log.keys():
                if key in self.timestamp_keys:
                    key_bigquery_schema_field = bigquery.SchemaField(key, 'TIMESTAMP')
                else:
                    key_bigquery_schema_field = bigquery.SchemaField(key, 'STRING')
                if not table_schema.__contains__(key_bigquery_schema_field):
                    table_schema.append(key_bigquery_schema_field)
        else:
            self.all_event_type_bigquery_schema[event_type] = self.get_table_schema_from_log(log)

    def populate_all_logs_dict(self, log):

        event_type = self.relace_chars_in_event_type(log["event_type"])
        if event_type in self.all_logs_on_event_type:
            logs = self.all_logs_on_event_type[event_type]
            if logs is not None:
                self.all_logs_on_event_type[event_type].append(log)
            else:
                self.all_logs_on_event_type[event_type] = [log]
        else:
            self.all_logs_on_event_type[event_type] = [log]

    def relace_chars_in_event_type(self, event_type):
        return event_type.replace(" ", "_").replace("-", "_")

    def write_log_types_to_indivitual_files(self, log_file_name):

        for event_type, logs in self.all_logs_on_event_type.items():
            flatten_file_name = self.get_event_type_flatten_file_name(event_type, log_file_name)
            local_data_file_name = os.path.join(self.flatten_data_file_location,
                                                flatten_file_name)
            with open(local_data_file_name, 'a+') as event_type_file:
                if logs is not None:
                    for log in logs:
                        event_type_file.writelines(json.dumps(log) + '\n')
            event_type_file.close()

    def get_event_type_flatten_file_name(self, event_type, log_file_name=None):

        if log_file_name is not None:
            flattened_file_date = self.extract_date_from_flattened_file_name(log_file_name)
            return event_type + "_" + flattened_file_date + "_" + self.start_date + "_" + self.end_date +".json"
        else:
            return event_type + "_"


    def extract_date_from_flattened_file_name(self, log_file_name):
        m = re.search('_\w\w\w\w-\w\w-\w\w_', log_file_name)
        if m is not None:
            flatten_file_date = m.group(0).replace('_', '')
            if flatten_file_date is not None:
                return flatten_file_date
            else:
                return datetime.now().__format__("%Y-%m-%d")
        else:
            return datetime.now().__format__("%Y%-m-%d")


    def wait_for_job(self, job):
        while True:
            job.reload()
            print("{}-{}".format(job.name, job.state))
            if job.state == 'DONE':
                if job.error_result:
                    print 'Error in job execution! error result: {} error message: {} '.format(job.error_result,
                                                                                               job.errors)
                else:
                    print("{}-{}".format(job.name, job.state))
                return
            time.sleep(10)

    def get_table_schema_from_log(self, log):

        table_schema = []
        for key in log.keys():
            if key in self.timestamp_keys:
                table_schema.append(bigquery.SchemaField(key, 'TIMESTAMP'))
            else:
                table_schema.append(bigquery.SchemaField(key, 'STRING'))

        return table_schema

    def gz_flatten_log_files_load_to_gcs_load_to_bigquery_from_gcs(self):
        print('Starting flattened file load to BQ')
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
            for (directory_path, directory_names, file_names) in walk(self.flatten_data_file_location):
                future = {executor.submit(self.convert_to_gz_file_and_load_to_gcs,
                                                  os.path.join(directory_path, file_name), file_name):
                                      file_name for file_name in file_names}
                print("End of process of job submission at: %s " % datetime.now())

    def convert_to_gz_file_and_load_to_gcs(self, source_file_name_with_path, source_file_name):
        print('Starting GZiping the file: {}'.format(source_file_name_with_path))
        destination_file_name_with_path = source_file_name_with_path + ".gz"
        with open(source_file_name_with_path, 'rb') as f_in, gzip.open(destination_file_name_with_path, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
            f_in.close()
            f_out.close()
            print('Done GZiping the file: {}'.format(source_file_name_with_path))
            os.remove(source_file_name_with_path)
            print('Removed: {}'.format(source_file_name_with_path))
            gcs_file_name = self.write_to_google_storage(destination_file_name_with_path, source_file_name)
            os.remove(destination_file_name_with_path)
            print('Removed GZ: {}'.format(destination_file_name_with_path))
            print self.load_bigquery_from_gcs(gcs_file_name)

    def write_to_google_storage(self, local_data_file_name_with_path, file_name):
        print('Starting GCS upload... file: {}'.format(local_data_file_name_with_path))
        credentials, project = auth.default()
        storage_client = storage.Client(self.google_bigquery_project_name, credentials)
        gcs_bucket = storage_client.get_bucket(self.google_storage_bucket_name)
        gcs_object = gcs_bucket.blob(self.get_gcs_bucket_name(file_name))
        gcs_object.upload_from_filename(local_data_file_name_with_path, content_type='application/gzip')

        gcs_file_name = "gs://{}/{}".format(gcs_object.bucket.name, gcs_object.name)
        logging.info('GCS file {} uploaded!'.format(gcs_file_name))
        print('Done GCS upload... file: {} to GCS file: '.format(local_data_file_name_with_path, gcs_file_name))
        return gcs_file_name

    def get_gcs_bucket_name(self, file_name):
        return '{}_{}_{}/{}'.format(self.platform_name_project_id, self.start_date, self.end_date, file_name)

    def load_bigquery_from_gcs(self, storage_file_name):
        print('Starting BQ load for GCS file: {} project: {}'.format(storage_file_name,
                                                                     self.google_bigquery_project_name))
        table_name = ""
        current_event_type = ""
        credentials, project = auth.default()
        bigquery_client = bigquery.Client(self.google_bigquery_project_name, credentials)
        data_set = bigquery_client.dataset(self.google_bigquery_data_set)

        for event_type in self.all_event_types:
            expected_storage_file_name = '/{}'.format(self.get_event_type_flatten_file_name(event_type, storage_file_name))
            if storage_file_name.__contains__(expected_storage_file_name):
                table_name = '{}_{}'.format(self.platform_name_project_id, event_type)
                table_name = table_name.lower()
                current_event_type = event_type
                print('Starting load_bigquery table_name: {} with storage file name: {}'.format(table_name,
                                                                                                storage_file_name))
                table = data_set.table(table_name)
                table_with_partition = data_set.table(
                    table_name + "$" +
                    datetime.strptime(self.extract_date_from_flattened_file_name(storage_file_name), '%Y-%m-%d').
                    strftime("%Y%m%d"))
                print "Table name with partition: {}".format(table_with_partition.name)
                try:
                    if not table.exists(bigquery_client):
                        table_schema = self.all_event_type_bigquery_schema[current_event_type]
                        table.schema = table_schema
                        table.partitioning_type = 'DAY'
                        table.create(bigquery_client)

                    else:
                        table.reload()
                        table_schema = table.schema
                        table_schema_new = self.all_event_type_bigquery_schema[current_event_type]
                        table_schema_with_new_fields = table.schema
                        for field in table_schema_new:
                            if field not in table_schema_with_new_fields:
                                table_schema_with_new_fields.append(field)
                        if not cmp(table_schema,table_schema_with_new_fields) == 0:
                            print("Table {} schema reloaded! ".format(table_name))
                            table.schema = table_schema_with_new_fields
                            table.partitioning_type = 'DAY'
                            table.update(bigquery_client)
                except:
                    tb = traceback.format_exc()
                    print('Error in finding table error: {}'.format(tb))

                table_with_partition.reload()
                job_name = 'viu-amplitude-{}-{}'.format(str(uuid.uuid4()), table_name)
                job = bigquery_client.load_table_from_storage(job_name, table_with_partition, storage_file_name)
                job.source_format = 'NEWLINE_DELIMITED_JSON'
                job.write_disposition = 'WRITE_APPEND'  # append to existing partition

                job.begin()
                self.wait_for_job(job)
                print('Loaded {} rows in {}:{}.'.format(job.output_rows, data_set.name, table_name))

    def exit_handler(self):
        print("For Platform: {} ID: {}".format(self.amplitude_api_platform, self.amplitude_api_project_id))


print("Starting Time: %s " % datetime.now())
reload(sys)
sys.setdefaultencoding('utf-8')

amplitudeApiData = AmplitudeApiData()

data_file_name_with_path = amplitudeApiData.get_data_from_api()

amplitudeApiData.extract_data_file(data_file_name_with_path,
                                   amplitudeApiData.extracted_file_location)

amplitudeApiData.extract_gz_file_from_extracted_folder()

amplitudeApiData.flatten_source_files()

amplitudeApiData.gz_flatten_log_files_load_to_gcs_load_to_bigquery_from_gcs()


print "Ending Time: %s " % datetime.now()

# atexit.register(amplitudeApiData.exit_handler())
print 'Done with processing'
