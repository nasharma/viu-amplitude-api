

--Android Data--

with uniques_shown_offer_popup as (

	select date(event_time) as date, count(distinct amplitude_id) as Uniques_shown_offer_popup
	from app140681.page_view
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_pageid = 'offer_activation'
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1

)

, new_user_activations as (

	select date(event_time) as date, count(distinct amplitude_id) as New_user_activations
	from app140681.subscription
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_subs_mode in ('trial', 'premium')
	and e_subs_status = 'success'
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1
)

, daily_repeat_users as (

	select date(event_time) as date, count(distinct amplitude_id) as daily_repeat_users
	from app140681.app_launch
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, video_stream_mins as (

	select date(event_time) as date, round((sum(1.0*e_play_duration))/60,2) as video_stream_mins
	from app140681.video_stream
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, download_and_play_mins as (

	select date(event_time) as date, round((sum(1.0*e_play_duration))/60,0) as download_and_play_mins
	from app140681.video_play
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, dau_with_video_views_video_stream as (

	select date(event_time) as date, count(distinct amplitude_id) as dau_with_video_views_video_stream
	from app140681.video_stream
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, dau_with_video_views_video_play as (

	select date(event_time) as date, count(distinct amplitude_id) as dau_with_video_views_video_play
	from app140681.video_play
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)
	
, final_data as (

	select to_char(a.date,'dd-Mon-yy') as date,
	a.Uniques_shown_offer_popup,
	b.new_user_activations, 
	c.daily_repeat_users, d.video_stream_mins, 
	e.download_and_play_mins, 
	f.dau_with_video_views_video_stream, 
	g.dau_with_video_views_video_play
	from uniques_shown_offer_popup a
	left join
	new_user_activations b
	on a.date = b.date
	left join
	daily_repeat_users c
	on a.date = c.date
	left join
	video_stream_mins d
	on a.date = d.date
	left join
	download_and_play_mins e
	on a.date = e.date
	left join
	dau_with_video_views_video_stream f
	on a.date = f.date
	left join
	dau_with_video_views_video_play g
	on a.date = g.date
	
)

, final_data1 as (

	select * from final_data
	union
	select 'Total' as date, sum(uniques_shown_offer_popup) as uniques_shown_offer_popup,
	sum(new_user_activations) as new_user_activations, sum(daily_repeat_users) as daily_repeat_users,
	sum(video_stream_mins) as video_stream_mins, sum(download_and_play_mins) as download_and_play_mins,
	sum(dau_with_video_views_video_stream) as dau_with_video_views_video_stream,
	sum(dau_with_video_views_video_play) as dau_with_video_views_video_play
	from final_data
	
)

select *, row_number() over (order by date) r_num 
from final_data1
order by 1;