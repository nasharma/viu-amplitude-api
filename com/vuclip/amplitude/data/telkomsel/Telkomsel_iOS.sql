--iOS Data--

with uniques_shown_offer_popup as (

	select date(event_time) as date, count(distinct amplitude_id) as uniques_shown_offer_popup
	from app141721.page_view
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_pageid = 'offer_activation'
	and country = 'Indonesia'
	and json_extract_path_text(event_properties, 'campaign_name') in ('tmsel', 'carrier.72', 'tmsel_postpaid')
	group by 1
	order by 1

)

, new_user_activations as (

	select date(event_time) as date, count(distinct amplitude_id) as New_user_activations
	from app141721.subscription
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_subs_mode in ('trial', 'premium')
	and e_subs_status = 'success'
	and json_extract_path_text(event_properties, 'campaign_name') in ('tmsel', 'carrier.72', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1
	
)

, final_data as (

	select to_char(a.date,'dd-Mon-yy') as date, a.uniques_shown_offer_popup, b.New_user_activations
	from uniques_shown_offer_popup a
	left join
	New_user_activations b
	on a.date = b.date
	
)
	
, final_data1 as (

	select * from final_data
	union
	select 'Total' as date, 
	sum(uniques_shown_offer_popup) as uniques_shown_offer_popup,
	sum(New_user_activations) as New_user_activations
	from final_data
	
)

select *, row_number() over (order by date) r_num 
from final_data1 
order by 1;