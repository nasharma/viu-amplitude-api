--Android video mins Mobile vs. Wifi--

with mobile_video_mins as (

	select date(event_time) as date, round((sum(1.0*e_play_duration))/60,0) as mobile_video_mins
	from app140681.video_stream
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	and lower(e_network) not in ('wifi', 'offline')
	group by 1
	order by 1
	
)

, wifi_video_mins as (

	select date(event_time) as date, round((sum(1.0*e_play_duration))/60,0) as wifi_video_mins
	from app140681.video_stream
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	and lower(e_network) in ('wifi', 'offline')
	group by 1
	order by 1
	
)

, total_wifi_mobile_mins as (
	
	select date(event_time) as date, round((sum(1.0*e_play_duration))/60,0) as total_video_mins
	from app140681.video_stream
	where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
	and date(event_time) < CURRENT_DATE::date
	and e_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and u_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1

)

, mobile_wifi_total_combined as (

	select to_char(a.date,'dd-Mon-yy') as date, a.mobile_video_mins,
	b.wifi_video_mins, c.total_video_mins
	from mobile_video_mins a
	left join
	wifi_video_mins b
	on a.date = b.date
	left join
	total_wifi_mobile_mins c
	on a.date = c.date
	
)

, percentage_data as (
	
	--select *, round((mobile_video_mins/total_video_mins)*100, 1)::text||'%' as mobile_video_percentage,
	--round((wifi_video_mins/total_video_mins)*100, 1)::text||'%' as wifi_video_percentage
	select *, round((mobile_video_mins/total_video_mins), 3) as mobile_video_percentage,
	round((wifi_video_mins/total_video_mins), 3) as wifi_video_percentage
	from mobile_wifi_total_combined
	group by 1,2,3,4

)

, total_data as (

	select * from percentage_data
	union
	select 'Total' as date, sum(mobile_video_mins) as mobile_video_mins,
	sum(wifi_video_mins) as wifi_video_mins, sum(total_video_mins) as total_video_mins,
	null as mobile_video_percentage, null as wifi_video_percentage
	from percentage_data

)

select *, row_number() over (order by date) r_num 
from total_data 
order by 1;