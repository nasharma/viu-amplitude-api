import psycopg2
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP
import smtplib
import sys
import time
from datetime import date, timedelta
import string
import subprocess
from subprocess import Popen, PIPE
import xlsxwriter

try:
	#Connecting to the Amplitude Redshift database using psycopg2 package
	connRS = psycopg2.connect("dbname='vuclip' user='ph_user4' host='vuclip.redshift.amplitude.com' port=5439  password='nnjMVouPGSoZRUAMT3udsa4N'")
	print ("Database connection established successfully with vuclip.redshift.amplitude.com server" )
	cur = connRS.cursor()
	
	#Initializing the starting row for each grid
	AndChartStartRow = 2
	iOSChartStartRow = 17
	WapChartStartRow = 32
	AndConsumptionChartStartRow = 47
	WapConsumptionChartStartRow = 62
	AndMvWChartStartRow = 77
	WiFiChartStartRow = 92
	AndHeaderRow = 107
	AndGridStartRow = 108
	iOSHeaderRow = 117
	iOSGridStartRow = 118
	WapHeaderRow = 122
	WapGridStartRow = 123
	AndMvWHeaderRow = 130
	AndMvWGridStartRow = 131
	WiFiHeaderRow = 138
	WiFiGridStartRow = 139
	
	#Getting the processing date and processing month
	processingDate = date.today() - timedelta(1)
	processingMonth = processingDate.replace(day=1)
	
	numOfDays = (processingDate - processingMonth).days
	
	if numOfDays < 15:
		chartWidth = 1250
	else :
		chartWidth = 1250 + (numOfDays - 15)*90
	
	processingDate = processingDate.strftime('%x')
	#processingMonth = processingMonth.strftime('%x')
	processingMonth = '05/27/17'
	
	fileNameDate = date.today()
	fileNameDate = fileNameDate.strftime('%x')
	
	query = '''select '01 - ' || to_char(to_date('processingDate','mm/dd/yy'),'DD Mon YYYY') date_'''
	query = string.replace(query, 'processingDate', processingDate)
	cur.execute(query)
	headerDate_row = cur.fetchall()
	for headerDate_tup in headerDate_row:
		headerDate = ''.join(headerDate_tup)	
	
	query = '''select to_char(to_date('fileNameDate','mm/dd/yy'),'DD Mon YY') date_'''
	query = string.replace(query, 'fileNameDate', fileNameDate)
	cur.execute(query)
	fileNameDate_row = cur.fetchall()
	for fileNameDate_tup in fileNameDate_row:
		fileNameDate = ''.join(fileNameDate_tup)
	
	print 'Processing Date: ' + processingDate + ' and Processing Month: ' + processingMonth
	
	#Andorid code starts here
	#Replacing date parameter with current month start date as the reports are MTD
	query = open(str('Telkomsel_Android.sql'),"rb").read()
	query = string.replace(query, 'dateParam', processingMonth)
	
	#Writing the executed query to Telkomsel_Android_ExecutedQuery.txt file in the same folder as the python script
	f= open("Telkomsel_Android_ExecutedQuery.txt","w+")
	f.write(str(query))
	f.close()
	
	cur.execute(query)
	rows = cur.fetchall()
	
	workbookName = 'Daily Update - Telkomsel Partnership Report ' + fileNameDate + '.xlsx'
	workbook = xlsxwriter.Workbook(workbookName)
	worksheetVideoMax = workbook.add_worksheet('VideoMax')
	
	#Setting the styling for workbook
	worksheetVideoMax.set_column('A:A', 32)
	worksheetVideoMax.set_column('B:AG', 12)
	numFormat = workbook.add_format({
										'font_size': 10,
										'font_name': 'Mucho Sans',
										'border': 1,
										'num_format': '#,###'
									})
	percNumFormat = workbook.add_format({
										'font_size': 10,
										'font_name': 'Mucho Sans',
										'border': 1,
										'num_format': '##%'
									})								
	cellFormat = workbook.add_format({
										'font_size': 10,
										'font_name': 'Mucho Sans',
										'border': 1
									})
	tncFormat = workbook.add_format({
										'font_size': 10,
										'font_name': 'Mucho Sans'
									})
	tncNumFormat = workbook.add_format({
										'font_size': 10,
										'font_name': 'Mucho Sans',
										'num_format': '#,###'
									 })
	tncHeaderFormat = workbook.add_format({
										'font_size': 10,
										'font_name': 'Mucho Sans',
										'bold': True
									})
	gridHeaderFormat = workbook.add_format({
										'font_size': 12,
										'font_name': 'Mucho Sans',
										'bold': True
									})
	headerFormat = workbook.add_format({
										'bold': True,
										'font_size': 32,
										'font_name': 'Mucho Sans'
										})
	date_format = workbook.add_format({ 
										'font_size': 10,
										'font_name': 'Mucho Sans',
										'border': 1,
										'bold': True,
										'align': 'right'
									 })
	
	#Writing the Sheet Header and inserting image in the header
	worksheetVideoMax.write(0, 2, 'Viu VideoMax Report (' + headerDate + ')', headerFormat)
	worksheetVideoMax.insert_image('B1', 'viu.png', {'x_scale': 0.5, 'y_scale': 0.18})
	
	worksheetVideoMax.write(AndHeaderRow, 0, 'Android', gridHeaderFormat)
	
	#Getting the column names (headers) and writing it in the workbook
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	x = 0 
	colHeader = ""
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="uniques_shown_offer_popup": colHeader = "Uniques shown offer popup"
		if colnames[x] =="new_user_activations": colHeader = "New User Activations"
		if colnames[x] =="daily_repeat_users": colHeader = "Daily Repeat Users"
		if colnames[x] =="video_stream_mins": colHeader = "Video Stream Mins"
		if colnames[x] =="download_and_play_mins": colHeader = "Download & Play Mins"
		if colnames[x] =="dau_with_video_views_video_stream": colHeader = "DAU with Video views - Video Stream"
		if colnames[x] =="dau_with_video_views_video_play": colHeader = "DAU with Video Views - Video Play"
		
		worksheetVideoMax.write(x + AndGridStartRow, 0, colHeader, cellFormat)
		x = x + 1
	
	#Writing the actual data in the workbook
	row = AndGridStartRow
	totalRows = 0
	
	for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, download_and_play_mins, dau_with_video_views_video_stream, dau_with_video_views_video_play, rn in rows:
		worksheetVideoMax.write(row, rn, date, date_format)
		worksheetVideoMax.write(row + 1, rn, uniques_shown_offer_popup, numFormat)
		worksheetVideoMax.write(row + 2, rn, new_user_activations, numFormat)
		worksheetVideoMax.write(row + 3, rn, daily_repeat_users, numFormat)
		worksheetVideoMax.write(row + 4, rn, video_stream_mins, numFormat)
		worksheetVideoMax.write(row + 5, rn, download_and_play_mins, numFormat)
		worksheetVideoMax.write(row + 6, rn, dau_with_video_views_video_stream, numFormat)
		worksheetVideoMax.write(row + 7, rn, dau_with_video_views_video_play, numFormat)
		
		if totalRows < rn: totalRows = rn
	
	print 'Android Total Rows: ' + str(totalRows)
	worksheetAndroidAppUsersChart = workbook.add_worksheet('AndroidAppUsersChart')
	
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	
	x = 0 
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="uniques_shown_offer_popup": colHeader = "Uniques shown offer popup"
		if colnames[x] =="new_user_activations": colHeader = "New User Activations"
		if colnames[x] =="daily_repeat_users": colHeader = "Daily Repeat Users"
		if colnames[x] =="video_stream_mins": colHeader = "Video Stream Mins"
		if colnames[x] =="download_and_play_mins": colHeader = "Download & Play Mins"
		if colnames[x] =="dau_with_video_views_video_stream": colHeader = "DAU with Video views - Video Stream"
		if colnames[x] =="dau_with_video_views_video_play": colHeader = "DAU with Video Views - Video Play"
		worksheetAndroidAppUsersChart.write(0, x, colHeader, cellFormat)
		x = x + 1

	row = 1
	col = 0

	for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, download_and_play_mins, dau_with_video_views_video_stream, dau_with_video_views_video_play, rn in rows:
		worksheetAndroidAppUsersChart.write(row, col, date, date_format)
		worksheetAndroidAppUsersChart.write(row, col + 1, uniques_shown_offer_popup, numFormat)
		worksheetAndroidAppUsersChart.write(row, col + 2, new_user_activations, numFormat)
		worksheetAndroidAppUsersChart.write(row, col + 3, daily_repeat_users, numFormat)
		worksheetAndroidAppUsersChart.write(row, col + 4, video_stream_mins, numFormat)
		worksheetAndroidAppUsersChart.write(row, col + 5, download_and_play_mins, numFormat)
		worksheetAndroidAppUsersChart.write(row, col + 6, dau_with_video_views_video_stream, numFormat)
		worksheetAndroidAppUsersChart.write(row, col + 7, dau_with_video_views_video_play, numFormat)
		
		row = row + 1
	
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=AndroidAppUsersChart!$B$2:$B$' + str(totalRows),
		#'values': '=VideoMax!$A$' + str((AndGridStartRow+1)) + ':$' + currMetricColumn + '$' + str(AndGridStartRow+4),
		'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
		#'categories': '=VideoMax!$A$' + str((AndGridStartRow+2)) + ':$A$' + str(AndGridStartRow+4),
		'marker': {'type': 'square', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndroidAppUsersChart!$B$1',
		'line':   {'color': '#ed7d31'}
	})
	
	chart.add_series({
		'values': '=AndroidAppUsersChart!$C$2:$C$' + str(totalRows),
		'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'square', 'border': {'color': '#ffc000'}, 'fill':   {'color': '#ffc000'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndroidAppUsersChart!$C$1',
		'line':   {'color': '#ffc000'}
	})
	
	chart.add_series({
		'values': '=AndroidAppUsersChart!$D$2:$D$' + str(totalRows),
		'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'square', 'border': {'color': '#70ad47'}, 'fill':   {'color': '#70ad47'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndroidAppUsersChart!$D$1',
		'line':   {'color': '#70ad47'}
	})
	
	chart.set_title({'name': 'Android App Users', 'font_color': 'Grey'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(AndChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
	
	#iOS code starts here
	#Replacing date parameter with current month start date as the reports are MTD
	query = open(str('Telkomsel_iOS.sql'),"rb").read()
	query = string.replace(query, 'dateParam', processingMonth)
	
	#Writing the executed query to Telkomsel_iOS_ExecutedQuery.txt file in the same folder as the python script
	f= open("Telkomsel_iOS_ExecutedQuery.txt","w+")
	f.write(str(query))
	f.close()
	
	cur.execute(query)
	rows = cur.fetchall()
	
	worksheetVideoMax.write(iOSHeaderRow, 0, 'iOS', gridHeaderFormat)
	
	#Getting the column names (headers) and writing it in the workbook
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	x = 0 
	colHeader = ""
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="uniques_shown_offer_popup": colHeader = "Uniques shown offer popup"
		if colnames[x] =="new_user_activations": colHeader = "New User Activations"
		
		worksheetVideoMax.write(x + iOSGridStartRow, 0, colHeader, cellFormat)
		x = x + 1
	
	#Writing the actual data in the workbook
	row = iOSGridStartRow
	totalRows = 0
	
	for date, uniques_shown_offer_popup, new_user_activations, rn in rows:
		worksheetVideoMax.write(row, rn, date, date_format)
		worksheetVideoMax.write(row + 1, rn, uniques_shown_offer_popup, numFormat)
		worksheetVideoMax.write(row + 2, rn, new_user_activations, numFormat)
		
		if totalRows < rn: totalRows = rn
	
	print 'iOS Total Rows: ' + str(totalRows)
	worksheetiOSAppUsersChart = workbook.add_worksheet('iOSAppUsersChart')
	
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	
	x = 0 
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="uniques_shown_offer_popup": colHeader = "Uniques shown offer popup"
		if colnames[x] =="new_user_activations": colHeader = "New User Activations"
		
		worksheetiOSAppUsersChart.write(0, x, colHeader, cellFormat)
		x = x + 1

	row = 1
	col = 0

	for date, uniques_shown_offer_popup, new_user_activations, rn in rows:
		worksheetiOSAppUsersChart.write(row, col, date, date_format)
		worksheetiOSAppUsersChart.write(row, col + 1, uniques_shown_offer_popup, numFormat)
		worksheetiOSAppUsersChart.write(row, col + 2, new_user_activations, numFormat)
		
		row = row + 1
	
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=iOSAppUsersChart!$B$2:$B$' + str(totalRows),
		'categories': '=iOSAppUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#a5a5a5'}, 'fill':   {'color': '#a5a5a5'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=iOSAppUsersChart!$B$1',
		'line':   {'color': '#a5a5a5'}
	})
	
	chart.add_series({
		'values': '=iOSAppUsersChart!$C$2:$C$' + str(totalRows),
		'categories': '=iOSAppUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=iOSAppUsersChart!$C$1',
		'line':   {'color': '#ed7d31'}
	})
	
	chart.set_title({'name': 'iOS App Users'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(iOSChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
	
	#WAP code starts here
	#Replacing date parameter with current month start date as the reports are MTD
	query = open(str('Telkomsel_wap.sql'),"rb").read()
	query = string.replace(query, 'dateParam', processingMonth)
	
	#Writing the executed query to Telkomsel_wap_ExecutedQuery.txt file in the same folder as the python script
	f= open("Telkomsel_wap_ExecutedQuery.txt","w+")
	f.write(str(query))
	f.close()
	
	cur.execute(query)
	rows = cur.fetchall()
	
	worksheetVideoMax.write(WapHeaderRow, 0, 'WAP', gridHeaderFormat)
	
	#Getting the column names (headers) and writing it in the workbook
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	x = 0 
	colHeader = ""
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="uniques_shown_offer_popup": colHeader = "Uniques shown offer popup"
		if colnames[x] =="new_user_activations": colHeader = "New User Activations"
		if colnames[x] =="daily_repeat_users": colHeader = "Daily Repeat Users"
		if colnames[x] =="video_stream_mins": colHeader = "Video Stream Mins"
		if colnames[x] =="dau_with_video_views": colHeader = "DAU with Video views"
		
		worksheetVideoMax.write(x + WapGridStartRow, 0, colHeader, cellFormat)
		x = x + 1
	
	#Writing the actual data in the workbook
	row = WapGridStartRow
	totalRows = 0
	
	for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, dau_with_video_views, rn in rows:
		worksheetVideoMax.write(row, rn, date, date_format)
		worksheetVideoMax.write(row + 1, rn, uniques_shown_offer_popup, numFormat)
		worksheetVideoMax.write(row + 2, rn, new_user_activations, numFormat)
		worksheetVideoMax.write(row + 3, rn, daily_repeat_users, numFormat)
		worksheetVideoMax.write(row + 4, rn, video_stream_mins, numFormat)
		worksheetVideoMax.write(row + 5, rn, dau_with_video_views, numFormat)
		
		if totalRows < rn: totalRows = rn
	
	print 'WAP Total Rows: ' + str(totalRows)
	worksheetWAPUsersChart = workbook.add_worksheet('WAPUsersChart')
	
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	
	x = 0 
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="uniques_shown_offer_popup": colHeader = "Uniques shown offer popup"
		if colnames[x] =="new_user_activations": colHeader = "New User Activations"
		if colnames[x] =="daily_repeat_users": colHeader = "Daily Repeat Users"
		if colnames[x] =="video_stream_mins": colHeader = "Video Stream Mins"
		if colnames[x] =="dau_with_video_views": colHeader = "DAU with Video views"
		
		worksheetWAPUsersChart.write(0, x, colHeader, cellFormat)
		x = x + 1

	row = 1
	col = 0

	for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, dau_with_video_views, rn in rows:
		worksheetWAPUsersChart.write(row, col, date, date_format)
		worksheetWAPUsersChart.write(row, col + 1, uniques_shown_offer_popup, numFormat)
		worksheetWAPUsersChart.write(row, col + 2, new_user_activations, numFormat)
		worksheetWAPUsersChart.write(row, col + 3, daily_repeat_users, numFormat)
		worksheetWAPUsersChart.write(row, col + 4, video_stream_mins, numFormat)
		worksheetWAPUsersChart.write(row, col + 5, dau_with_video_views, numFormat)
		
		row = row + 1
	
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=WAPUsersChart!$B$2:$B$' + str(totalRows),
		'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'square', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=WAPUsersChart!$B$1',
		'line':   {'color': '#ed7d31'}
	})
	
	chart.add_series({
		'values': '=WAPUsersChart!$C$2:$C$' + str(totalRows),
		'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'square', 'border': {'color': '#ffc000'}, 'fill':   {'color': '#ffc000'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=WAPUsersChart!$C$1',
		'line':   {'color': '#ffc000'}
	})
	
	chart.add_series({
		'values': '=WAPUsersChart!$D$2:$D$' + str(totalRows),
		'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'square', 'border': {'color': '#70ad47'}, 'fill':   {'color': '#70ad47'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=WAPUsersChart!$D$1',
		'line':   {'color': '#70ad47'}
	})
	
	chart.set_title({'name': 'Mobile Browser Users'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(WapChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
		
	#Android Consumption Chart
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=AndroidAppUsersChart!$E$2:$E$' + str(totalRows),
		'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'square', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndroidAppUsersChart!$E$1',
		'line':   {'color': '#ed7d31'},
		'trendline': {'type': 'linear', 'line': {
												'color': '#ed7d31',
												'width': 1,
												'dash_type': 'long_dash'
												}
					}
	})
	
	chart.add_series({
		'values': '=AndroidAppUsersChart!$F$2:$F$' + str(totalRows),
		'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#ffc000'}, 'fill':   {'color': '#ffc000'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndroidAppUsersChart!$F$1',
		'line':   {'color': '#ffc000'}
	})
	
	chart.set_title({'name': 'Android Consumption (Mins)'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(AndConsumptionChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
	
	#WAP Consumption Chart
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=WAPUsersChart!$E$2:$E$' + str(totalRows),
		'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=WAPUsersChart!$E$1',
		'line':   {'color': '#ed7d31'}
	})
	
	chart.set_title({'name': 'Mobile Browser Consumption (Mins)'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(WapConsumptionChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
	
	#Android Video Consumption on Mobile vs WiFi code starts here
	#Replacing date parameter with current month start date as the reports are MTD
	query = open(str('Telkomsel_Android_MvW.sql'),"rb").read()
	query = string.replace(query, 'dateParam', processingMonth)
	
	#Writing the executed query to Telkomsel_Android_MvW_ExecutedQuery.txt file in the same folder as the python script
	f= open("Telkomsel_Android_MvW_ExecutedQuery.txt","w+")
	f.write(str(query))
	f.close()
	
	cur.execute(query)
	rows = cur.fetchall()
	
	worksheetVideoMax.write(AndMvWHeaderRow, 0, 'Mobile Network Vs Wifi', gridHeaderFormat)
	
	#Getting the column names (headers) and writing it in the workbook
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	x = 0 
	colHeader = ""
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="mobile_video_mins": colHeader = "Mobile"
		if colnames[x] =="wifi_video_mins": colHeader = "WiFi"
		if colnames[x] =="total_video_mins": colHeader = "Total"
		if colnames[x] =="mobile_video_percentage": colHeader = "Mobile %"
		if colnames[x] =="wifi_video_percentage": colHeader = "WiFi %"
		
		worksheetVideoMax.write(x + AndMvWGridStartRow, 0, colHeader, cellFormat)
		x = x + 1
	
	#Writing the actual data in the workbook
	row = AndMvWGridStartRow
	totalRows = 0
	
	for date, mobile_video_mins, wifi_video_mins, total_video_mins, mobile_video_percentage, wifi_video_percentage, rn in rows:
		worksheetVideoMax.write(row, rn, date, date_format)
		worksheetVideoMax.write(row + 1, rn, mobile_video_mins, numFormat)
		worksheetVideoMax.write(row + 2, rn, wifi_video_mins, numFormat)
		worksheetVideoMax.write(row + 3, rn, total_video_mins, numFormat)
		worksheetVideoMax.write(row + 4, rn, mobile_video_percentage, percNumFormat)
		worksheetVideoMax.write(row + 5, rn, wifi_video_percentage, percNumFormat)
		
		if totalRows < rn: totalRows = rn
	
	print 'Android Mobile vs WiFi Consumption Total Rows: ' + str(totalRows)
	worksheetAndMvWUsersChart = workbook.add_worksheet('AndMvWUsersChart')
	
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	
	x = 0 
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="mobile_video_mins": colHeader = "Mobile"
		if colnames[x] =="wifi_video_mins": colHeader = "WiFi"
		if colnames[x] =="total_video_mins": colHeader = "Total"
		if colnames[x] =="mobile_video_percentage": colHeader = "Mobile %"
		if colnames[x] =="wifi_video_percentage": colHeader = "WiFi %"
		
		worksheetAndMvWUsersChart.write(0, x, colHeader, cellFormat)
		x = x + 1

	row = 1
	col = 0

	for date, mobile_video_mins, wifi_video_mins, total_video_mins, mobile_video_percentage, wifi_video_percentage, rn in rows:
		worksheetAndMvWUsersChart.write(row, col, date, date_format)
		worksheetAndMvWUsersChart.write(row, col + 1, mobile_video_mins, numFormat)
		worksheetAndMvWUsersChart.write(row, col + 2, wifi_video_mins, numFormat)
		worksheetAndMvWUsersChart.write(row, col + 3, total_video_mins, numFormat)
		worksheetAndMvWUsersChart.write(row, col + 4, mobile_video_percentage, percNumFormat)
		worksheetAndMvWUsersChart.write(row, col + 5, wifi_video_percentage, percNumFormat)
		
		row = row + 1
	
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=AndMvWUsersChart!$B$2:$B$' + str(totalRows),
		'categories': '=AndMvWUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#a3c7e7'}, 'fill':   {'color': '#a3c7e7'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndMvWUsersChart!$B$1',
		'line':   {'color': '#a3c7e7'}
	})
	
	chart.add_series({
		'values': '=AndMvWUsersChart!$C$2:$C$' + str(totalRows),
		'categories': '=AndMvWUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=AndMvWUsersChart!$C$1',
		'line':   {'color': '#ed7d31'}
	})
	
	chart.set_title({'name': 'Mobile Network Vs WiFi'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(AndMvWChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
	
	#WiFi code starts here
	#Replacing date parameter with current month start date as the reports are MTD
	query = open(str('Telkomsel_WiFi.sql'),"rb").read()
	query = string.replace(query, 'dateParam', processingMonth)
	
	#Writing the executed query to Telkomsel_WiFi_MvW_ExecutedQuery.txt file in the same folder as the python script
	f= open("Telkomsel_WiFi_ExecutedQuery.txt","w+")
	f.write(str(query))
	f.close()
	
	cur.execute(query)
	rows = cur.fetchall()
	
	worksheetVideoMax.write(WiFiHeaderRow, 0, 'Wifi - IP Based', gridHeaderFormat)
	
	#Getting the column names (headers) and writing it in the workbook
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	x = 0 
	colHeader = ""
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="wifi_ip_percentage": colHeader = "WiFi"
		
		worksheetVideoMax.write(x + WiFiGridStartRow, 0, colHeader, cellFormat)
		x = x + 1
	
	#Writing the actual data in the workbook
	row = WiFiGridStartRow
	totalRows = 0
	
	for date, wifi_ip_percentage, rn in rows:
		worksheetVideoMax.write(row, rn, date, date_format)
		worksheetVideoMax.write(row + 1, rn, wifi_ip_percentage, percNumFormat)
		
		if totalRows < rn: totalRows = rn
	
	print 'WiFi Total Rows: ' + str(totalRows)
	worksheetWiFiUsersChart = workbook.add_worksheet('WiFiUsersChart')
	
	colnames = [desc[0] for desc in cur.description]
	cols = len(colnames)
	
	x = 0 
	while x < cols - 1:
		if colnames[x] =="date": colHeader = "Date"
		if colnames[x] =="wifi_ip_percentage": colHeader = "WiFi"
		
		worksheetWiFiUsersChart.write(0, x, colHeader, cellFormat)
		x = x + 1

	row = 1
	col = 0

	for date, wifi_ip_percentage, rn in rows:
		worksheetWiFiUsersChart.write(row, col, date, date_format)
		worksheetWiFiUsersChart.write(row, col + 1, wifi_ip_percentage, percNumFormat)
		
		row = row + 1
	
	#Adding the chart
	chart = workbook.add_chart({'type': 'line'})
	
	# Add series to the chart
	chart.add_series({
		'values': '=WiFiUsersChart!$B$2:$B$' + str(totalRows),
		'categories': '=WiFiUsersChart!$A$2:$A$' + str(totalRows),
		'marker': {'type': 'circle', 'border': {'color': '#a3c7e7'}, 'fill':   {'color': '#a3c7e7'},},
		'data_labels': {'value': True, 'position': 'above'},
		'name': '=WiFiUsersChart!$B$1',
		'line':   {'color': '#a3c7e7'}
	})
	
	chart.set_title({'name': 'WiFi - IP Based'})
	
	chart.set_plotarea({
		'layout': {
			'x':      0.05,
			'y':      0.2,
			'width':  0.9,
			'height': 0.6,
		}
	})
	
	chart.set_legend({'position': 'bottom'})
	chart.set_size({'width': chartWidth, 'height': 276})
	
	chartPosition = 'A' + str(WiFiChartStartRow)
	worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})
	
	#Adding a sheet for Terms and Conditions and adding contents to it
	worksheetTnC = workbook.add_worksheet('Terms & Conditions')
	
	worksheetTnC.hide_gridlines(2)
	worksheetTnC.set_column('A:A', 1)
	worksheetTnC.set_column('B:B', 0.5)
	
	worksheetTnC.insert_image('A1', 'tncImageViu.png', {'x_scale': 1, 'y_scale': 1})
	
	worksheetTnC.write(3, 2, 'Terms & Conditions:', tncHeaderFormat)
	worksheetTnC.write(4, 0, '1', tncNumFormat)
	worksheetTnC.write(4, 2, 'This report is for usage of intended partnership recipients only & for restricted circulation only.', tncFormat)
	worksheetTnC.write(5, 0, '2', tncNumFormat)
	worksheetTnC.write(5, 2, 'Modification of this report is not permitted.', tncFormat)
	worksheetTnC.write(6, 0, '3', tncNumFormat)
	worksheetTnC.write(6, 2, 'All data in this report is highly confidential & to be treated in accordance with the terms of the signed agreement between the Partner and Vuclip or its authorised representatives.', tncFormat)
	worksheetTnC.write(7, 0, '4', tncNumFormat)
	worksheetTnC.write(7, 2, 'This report is the property of Vuclip Singapore Pte Ltd. & all trademarks are the sole property of respective owners.', tncFormat)
	worksheetTnC.write(8, 0, '5', tncNumFormat)
	worksheetTnC.write(8, 2, 'Editing, distortion, omission, conversion and/or reeinginering of this report is not permitted.', tncFormat)
	worksheetTnC.write(9, 0, '6', tncNumFormat)
	worksheetTnC.write(9, 2, 'Not permitted to be used for public display, or as a reference report for further analytics.', tncFormat)
	worksheetTnC.write(10, 0, '7', tncNumFormat)
	worksheetTnC.write(10, 2, 'Terms and conditions may be modified from time to time & for the latest terms please touch base with your Partnership Manager.', tncFormat)
	
	worksheetVideoMax.freeze_panes(1, 0)
	worksheetVideoMax.hide_gridlines(2)
	worksheetAndroidAppUsersChart.hide()
	worksheetiOSAppUsersChart.hide()
	worksheetWAPUsersChart.hide()
	worksheetAndMvWUsersChart.hide()
	worksheetWiFiUsersChart.hide()
	workbook.close()
	del cur
	connRS.close()
	
except ValueError :
	print "An error occurred during the processing. Closing the workbook"
	workbook.close()
	del cur
	connRS.close()

time.sleep(3)

print("Sending email to")

recipients = ['abhinav.vajpayee@vuclip.com']
#recipients = ['meri.djajakustio@vuclip.com','varun@vuclip.com','utsav@vuclip.com','tushar.harduley@vuclip.com','abhinav.vajpayee@vuclip.com','sidhant.das@vuclip.com','rahul.badki@vuclip.com']
emaillist = [elem.strip().split(',') for elem in recipients]
msg = MIMEMultipart()
msg['Subject'] = str('Telkomsel Daily Report - ' + fileNameDate)
msg['From'] = 'analytics@vuclip.com'
msg['Reply-to'] = 'analytics@vuclip.com'
msg['To'] = ", ".join(recipients)
 
msg.preamble = 'Multipart massage.\n'
 
part = MIMEText("Hi, \r\nPlease find attached the file containing Telkomsel Daily Report.")
msg.attach(part)
 
part = MIMEApplication(open(str(workbookName),"rb").read())
part.add_header('Content-Disposition', 'attachment', filename=str(workbookName))

msg.attach(part)

print  ", ".join(recipients)

p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
#p.communicate(msg.as_string())

print("Email sent successfully")
print("Ending the script")