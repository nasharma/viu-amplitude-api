select to_char(date(event_time),'dd-Mon-yy') as date,
round((sum(case when lower(e_network) in ('wifi', 'offline') then 1.0*e_play_duration end)/60)/(sum(1.0*e_play_duration)/60),2) as wifi_ip_percentage,
row_number() over (order by date) r_num 
from app140681.video_stream
where date(event_time) >= to_date('dateParam', 'mm/dd/yy')
and date(event_time) < CURRENT_DATE::date
and e_campaign_name = 'carrier.72'
and country = 'Indonesia'
and u_user_subs_partner = 'Telkomsel'
group by 1
order by 1