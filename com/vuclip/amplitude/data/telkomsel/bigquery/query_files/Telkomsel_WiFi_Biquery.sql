select *, row_number() over (order by DATE) r_num from ( select FORMAT_DATE('%d-%b-%y', DATE(event_time)) as date,
round((sum(case when lower(event_network) in ('wifi', 'offline') then 1.0*CAST(event_play_duration AS INT64) end)/60)/(sum(1.0*CAST(event_play_duration AS INT64))/60),2) as wifi_ip_percentage
from amplitude.android_140681_video_stream
where event_time >= TIMESTAMP(@startDate)
and event_time < TIMESTAMP(CURRENT_DATE())
and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
and event_campaign_name = 'carrier.72'
and country = 'Indonesia'
and user_user_subs_partner = 'Telkomsel'
group by 1
order by 1 ) as a;