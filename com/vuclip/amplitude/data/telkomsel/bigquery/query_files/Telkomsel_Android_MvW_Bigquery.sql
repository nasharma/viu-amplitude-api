--Android video mins Mobile vs. Wifi--

with mobile_video_mins as (

	select DATE(event_time) as date, round((sum(1.0*CAST(event_play_duration AS INT64)))/60,0) as mobile_video_mins
	from amplitude.android_140681_video_stream
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	and lower(event_network) not in ('wifi', 'offline')
	group by 1
	order by 1
	
)

, wifi_video_mins as (

	select DATE(event_time) as date, round((sum(1.0*CAST(event_play_duration AS INT64)))/60,0) as wifi_video_mins
	from amplitude.android_140681_video_stream
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	and lower(event_network) in ('wifi', 'offline')
	group by 1
	order by 1
	
)

, total_wifi_mobile_mins as (
	
	select DATE(event_time) as date, round((sum(1.0*CAST(event_play_duration AS INT64)))/60,0) as total_video_mins
	from amplitude.android_140681_video_stream
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1

)

, mobile_wifi_total_combined as (

	select  FORMAT_DATE('%d-%b-%y', a.date) as date, a.mobile_video_mins,
	b.wifi_video_mins, c.total_video_mins
	from mobile_video_mins a
	left join
	wifi_video_mins b
	on a.date = b.date
	left join
	total_wifi_mobile_mins c
	on a.date = c.date
	
)

, percentage_data as (
	
	--select *, round((mobile_video_mins/total_video_mins)*100, 1)::text||'%' as mobile_video_percentage,
	--round((wifi_video_mins/total_video_mins)*100, 1)::text||'%' as wifi_video_percentage
	select *, CONCAT(CAST(round((CAST(mobile_video_mins AS FLOAT64) * 100.0 /CAST(total_video_mins AS FLOAT64)), 1) as STRING), '%') as mobile_video_percentage,
	 CONCAT(CAST(round((CAST(wifi_video_mins AS FLOAT64) * 100.0 /CAST(total_video_mins AS FLOAT64)), 1) as STRING), '%') as wifi_video_percentage
	from mobile_wifi_total_combined
	group by 1,2,3,4

)

, total_data as (

	select * from percentage_data
	union ALL
	select 'Total' as date, sum(mobile_video_mins) as mobile_video_mins,
	sum(wifi_video_mins) as wifi_video_mins, sum(total_video_mins) as total_video_mins,
	null as mobile_video_percentage, null as wifi_video_percentage
	from percentage_data

)

select *, row_number() over (order by date) r_num 
from total_data 
order by 1;