--wap Data--
	
with uniques_shown_offer_popup as (
	
	select DATE(event_time) as date, count(distinct amplitude_id) as Uniques_shown_offer_popup
	from amplitude.mobile_143505_page_view
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_pageid = 'offer_activation'
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1
)

, new_user_activations as (

	select DATE(event_time) as date, count(distinct amplitude_id) as New_user_activations
	from amplitude.mobile_143505_subscription
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_subs_mode in ('trial', 'premium')
	and event_subs_status = 'success'
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1
)

, daily_repeat_users as (

	select DATE(event_time) as date, count(distinct amplitude_id) as daily_repeat_users
	from amplitude.mobile_143505_app_launch
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, video_stream_mins as (

	select DATE(event_time) as date, round((sum(1.0*CAST(event_play_duration AS INT64)))/60,0) as video_stream_mins
	from amplitude.mobile_143505_video_stream
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, dau_with_video_views as (

	select DATE(event_time) as date, count(distinct amplitude_id) as dau_with_video_views
	from amplitude.mobile_143505_video_stream
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, final_data as (

	select FORMAT_DATE('%d-%b-%y', a.date) as date, a.uniques_shown_offer_popup,
	b.new_user_activations, c.daily_repeat_users,
	d.video_stream_mins, e.dau_with_video_views
	from uniques_shown_offer_popup a
	left join
	new_user_activations b
	on a.date = b.date
	left join
	daily_repeat_users c
	on a.date = c.date
	left join
	video_stream_mins d
	on a.date = d.date
	left join
	dau_with_video_views e
	on a.date = e.date

)

, final_data1 as (

	select * from final_data
	union all
	select 'Total' as date, sum(uniques_shown_offer_popup) as uniques_shown_offer_popup,
	sum(new_user_activations) as new_user_activations, sum(daily_repeat_users) as daily_repeat_users,
	sum(video_stream_mins) as video_stream_mins, sum(dau_with_video_views) as dau_with_video_views
	from final_data
)

select date, uniques_shown_offer_popup,
	new_user_activations, daily_repeat_users,
	video_stream_mins, dau_with_video_views, row_number() over (order by date) r_num 
from final_data1
order by 1;