--iOS Data--

with uniques_shown_offer_popup as (

	select DATE(event_time) as date, count(distinct amplitude_id) as uniques_shown_offer_popup
	from amplitude.ios_141721_page_view
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_pageid = 'offer_activation'
	and country = 'Indonesia'
	and event_campaign_name in ('tmsel', 'carrier.72', 'tmsel_postpaid')
	group by 1
	order by 1

)

, new_user_activations as (

	select DATE(event_time) as date, count(distinct amplitude_id) as New_user_activations
	from amplitude.ios_141721_subscription
	where event_time >= TIMESTAMP(@startDate)
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP(@startDate)
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_subs_mode in ('trial', 'premium')
	and event_subs_status = 'success'
	and event_campaign_name  in ('tmsel', 'carrier.72', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1
	
)

, final_data as (

	select FORMAT_DATE('%d-%b-%y', a.date) as date, a.uniques_shown_offer_popup, b.New_user_activations
	from uniques_shown_offer_popup a
	left join
	New_user_activations b
	on a.date = b.date
	
)
	
, final_data1 as (

	select * from final_data
	union all
	select 'Total' as date, 
	sum(uniques_shown_offer_popup) as uniques_shown_offer_popup,
	sum(New_user_activations) as New_user_activations
	from final_data
	
)

select *, row_number() over (order by date) r_num 
from final_data1 
order by 1;