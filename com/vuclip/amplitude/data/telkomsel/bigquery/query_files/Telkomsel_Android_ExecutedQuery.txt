with uniques_shown_offer_popup as (

	select DATE(event_time) as date, count(distinct amplitude_id) as Uniques_shown_offer_popup
	from amplitude.android_140681_page_view
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_pageid = 'offer_activation'
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1

)

, new_user_activations as (

	select DATE(event_time) as date, count(distinct amplitude_id) as New_user_activations
	from amplitude.android_140681_subscription
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_subs_mode in ('trial', 'premium')
	and event_subs_status = 'success'
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	group by 1
	order by 1
)

, daily_repeat_users as (

	select DATE(event_time) as date, count(distinct amplitude_id) as daily_repeat_users
	from amplitude.android_140681_app_launch
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, video_stream_mins as (

	select DATE(event_time) as date, round((sum(1.0*CAST(event_play_duration AS INT64)))/60,2) as video_stream_mins
	from amplitude.android_140681_video_stream
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, download_and_play_mins as (

	select DATE(event_time) as date, round((sum(1.0*CAST(event_play_duration AS INT64)))/60,0) as download_and_play_mins
	from amplitude.android_140681_video_play
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, dau_with_video_views_video_stream as (

	select DATE(event_time) as date, count(distinct amplitude_id) as dau_with_video_views_video_stream
	from amplitude.android_140681_video_stream
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, dau_with_video_views_video_play as (

	select DATE(event_time) as date, count(distinct amplitude_id) as dau_with_video_views_video_play
	from amplitude.android_140681_video_play
	where event_time >= TIMESTAMP('2017-06-01')
	and event_time < TIMESTAMP(CURRENT_DATE())
	and _PARTITIONTIME >= TIMESTAMP('2017-06-01')
	and _PARTITIONTIME < TIMESTAMP(CURRENT_DATE())
	and event_campaign_name in ('carrier.72', 'tmsel', 'tmsel_postpaid')
	and country = 'Indonesia'
	and user_user_subs_partner = 'Telkomsel'
	group by 1
	order by 1
)

, final_data as (

	select FORMAT_DATE('%d-%b-%y', a.date) as COMPUTATION_DATE,
	a.Uniques_shown_offer_popup,
	b.new_user_activations, 
	c.daily_repeat_users, d.video_stream_mins, 
	e.download_and_play_mins, 
	f.dau_with_video_views_video_stream, 
	g.dau_with_video_views_video_play
	from uniques_shown_offer_popup a
	left join
	new_user_activations b
	on a.date = b.date
	left join
	daily_repeat_users c
	on a.date = c.date
	left join
	video_stream_mins d
	on a.date = d.date
	left join
	download_and_play_mins e
	on a.date = e.date
	left join
	dau_with_video_views_video_stream f
	on a.date = f.date
	left join
	dau_with_video_views_video_play g
	on a.date = g.date
	
)

, final_data1 as (

	select * from final_data
	union ALL
	select 'Total' as COMPUTATION_DATE, sum(uniques_shown_offer_popup) as uniques_shown_offer_popup,
	sum(new_user_activations) as new_user_activations, sum(daily_repeat_users) as daily_repeat_users,
	sum(video_stream_mins) as video_stream_mins, sum(download_and_play_mins) as download_and_play_mins,
	sum(dau_with_video_views_video_stream) as dau_with_video_views_video_stream,
	sum(dau_with_video_views_video_play) as dau_with_video_views_video_play
	from final_data
	
)

select *, row_number() over (order by COMPUTATION_DATE) r_num 
from final_data1
order by 1;