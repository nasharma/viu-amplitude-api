import uuid

import psycopg2
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP
import smtplib
import sys
import time
from datetime import date, timedelta
import string
import subprocess
from subprocess import Popen, PIPE
import xlsxwriter
from google import auth
from google.cloud import bigquery

default_project = 'vuclipdataflow-1301'

query_file_name = {
  'android': './query_files/Telkomsel_Android_Bigquery.sql',
  'android_mvw': './query_files/Telkomsel_Android_MvW_Bigquery.sql',
  'ios': './query_files/Telkomsel_iOS_Bigquery.sql',
  'wap': './query_files/Telkomsel_wap_Biquery.sql',
  'wifi': './query_files/Telkomsel_WiFi_Biquery.sql'
}

execution_query_file_name={
  'android':'./query_files/Telkomsel_Android_ExecutedQuery.txt',
  'android_mvw': './query_files/Telkomsel_Android_MvW_Bigquery_ExecutedQuery.txt',
  'ios': './query_files/Telkomsel_iOS_Bigquery_ExecutedQuery.txt',
  'wap': './query_files/Telkomsel_wap_Biquery_ExecutedQuery.txt',
  'wifi': './query_files/Telkomsel_WiFi_Biquery_ExecutedQuery.txt'
}


def get_bigquery_client(project_name):
  credentials, project = auth.default()
  return bigquery.Client(project_name, credentials)


def initialize_worksheet_row_numbers():
    global AndChartStartRow, AndHeaderRow, AndGridStartRow, iOSChartStartRow, iOSHeaderRow, iOSGridStartRow, \
        WapChartStartRow, WapHeaderRow, WapGridStartRow, AndConsumptionChartStartRow, WapConsumptionChartStartRow, \
        AndMvWChartStartRow, AndMvWHeaderRow, AndMvWGridStartRow, WiFiChartStartRow, WiFiHeaderRow, WiFiGridStartRow

    AndChartStartRow = 2
    AndHeaderRow = 107
    AndGridStartRow = 108
    iOSChartStartRow = 17
    iOSHeaderRow = 121
    iOSGridStartRow = 122
    WapChartStartRow = 32
    WapHeaderRow = 134
    WapGridStartRow = 135
    AndConsumptionChartStartRow = 47
    WapConsumptionChartStartRow = 62
    AndMvWChartStartRow = 77
    AndMvWHeaderRow = 147
    AndMvWGridStartRow = 148
    WiFiChartStartRow = 92
    WiFiHeaderRow = 155
    WiFiGridStartRow = 156


def get_end_date():
    return date.today() - timedelta(1)


def get_start_date():
    return end_date.replace(day=1)


def set_chart_width(numOfDays):
    global chartWidth
    if numOfDays < 15:
        chartWidth = 1250
    else:
        chartWidth = 1250 + (numOfDays - 15) * 90


def get_header_date(end_date_str):
    return '01 - {}'.format(end_date_str)


def get_query(query_type, start_date):
    query = open(str(query_file_name.get(query_type)), "rb").read()
    return query.replace('@startDate', '\'{}\''.format(start_date.strftime('%Y-%m-%d')))


def write_final_query_to_file(query_type):
    global f
    f = open(execution_query_file_name.get(query_type), "w+")
    f.write(str(query))
    f.close()

def wait_for_job(job):
    while True:
        job.reload()  # Refreshes the state via a GET request.
        if job.state == 'DONE':
            if job.error_result:
                raise RuntimeError(job.errors)
            return
        time.sleep(1)

def get_rows_from_query_results(query_results):
    rows, total_rows, page_token = query_results.fetch_data(
            max_results=31)
    return rows

def get_rows(query):
    client = get_bigquery_client(default_project)
    query_job = client.run_async_query(
        str(uuid.uuid4()),
        query,
        query_parameters=())
    query_job.use_legacy_sql = False
    query_job.begin()
    wait_for_job(query_job)
    return get_rows_from_query_results(query_job.results())


def get_android_column_names():
    colnames = []
    colnames.append("Date")
    colnames.append("Uniques shown offer popup")
    colnames.append("New User Activations")
    colnames.append("Daily Repeat Users")
    colnames.append("Video Stream Mins")
    colnames.append("Download & Play Mins")
    colnames.append("DAU with Video views - Video Stream")
    colnames.append("Premium Active base")
    colnames.append("Renewed user")
    colnames.append("Retained user")
    colnames.append("Churned user")
    return colnames

def get_ios_column_names():
    columns = []
    columns.append("Date")
    columns.append("Uniques shown offer popup")
    columns.append("New User Activations")
    columns.append("MAU")
    columns.append("New user activations")
    columns.append("Minutes")
    columns.append("MAU with VV")
    columns.append("Premium Active base")
    columns.append("Renewed user")
    columns.append("Retained user")
    columns.append("Churned user")
    return columns

def get_wap_column_names():
    columns = []
    columns.append("Date")
    columns.append("Uniques shown offer popup")
    columns.append("New User Activations")
    columns.append("Daily Repeat Users")
    columns.append("Video Stream Mins")
    columns.append("DAU with Video views")
    columns.append("Premium Active base")
    columns.append("Renewed user")
    columns.append("Retained user")
    columns.append("Churned user")
    return columns

def get_android_mvw_columns():
    columns = []
    columns.append("Date")
    columns.append("Mobile")
    columns.append("WiFi")
    columns.append("Total")
    columns.append("Mobile %")
    columns.append("WiFi %")
    return columns

def get_wifi_columns():
    columns = []
    columns.append("Date")
    columns.append("WiFi")
    return columns


def initiate_formates():
    global numFormat, percNumFormat, cellFormat, tncFormat, tncNumFormat, tncHeaderFormat, gridHeaderFormat, headerFormat, date_format
    numFormat = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans',
        'border': 1,
        'num_format': '#,###'
    })
    percNumFormat = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans',
        'border': 1,
        'num_format': '##%'
    })
    cellFormat = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans',
        'border': 1
    })
    tncFormat = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans'
    })
    tncNumFormat = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans',
        'num_format': '#,###'
    })
    tncHeaderFormat = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans',
        'bold': True
    })
    gridHeaderFormat = workbook.add_format({
        'font_size': 12,
        'font_name': 'Mucho Sans',
        'bold': True
    })
    headerFormat = workbook.add_format({
        'bold': True,
        'font_size': 32,
        'font_name': 'Mucho Sans'
    })
    date_format = workbook.add_format({
        'font_size': 10,
        'font_name': 'Mucho Sans',
        'border': 1,
        'bold': True,
        'align': 'right'
    })


try:
    #Connecting to the Amplitude Redshift database using psycopg2 package
    #connRS = psycopg2.connect("dbname='vuclip' user='ph_user4' host='vuclip.redshift.amplitude.com' port=5439  password='nnjMVouPGSoZRUAMT3udsa4N'")
    client = get_bigquery_client(default_project)
    print ("Database connection established successfully with bigquery")

    #Initializing the starting row for each grid

    initialize_worksheet_row_numbers()

    #Getting the processing date and processing month
    end_date = get_end_date()
    start_date = get_start_date()

    numOfDays = (end_date - start_date).days

    set_chart_width(numOfDays)

    end_date_str = end_date.strftime('%d %b %y')
    start_date_str = start_date.strftime('%d %b %y')
    start_date_str_for_query = start_date.strftime('%x')
    
    fileNameDate = date.today()
    fileNameDate = fileNameDate.strftime('%d %b %y')

    print 'Processing Start date: ' + start_date_str + ' and Processing End Date: ' + end_date_str

    query = get_query('android', start_date)

    write_final_query_to_file('android')

    rows = get_rows(query)

    workbookName = 'Daily Update - BigQuery - Telkomsel Partnership Report ' + fileNameDate + '.xlsx'
    workbook = xlsxwriter.Workbook(workbookName)
    worksheetVideoMax = workbook.add_worksheet('VideoMax')

    #Setting the styling for workbook
    worksheetVideoMax.set_column('A:A', 32)
    worksheetVideoMax.set_column('B:AG', 12)

    initiate_formates()

    #Writing the Sheet Header and inserting image in the header
    worksheetVideoMax.write(0, 2, 'Viu VideoMax Report (' + get_header_date(end_date_str) + ')', headerFormat)
    worksheetVideoMax.insert_image('B1', 'viu.png', {'x_scale': 0.5, 'y_scale': 0.18})

    worksheetVideoMax.write(AndHeaderRow, 0, 'Android', gridHeaderFormat)

    columns = get_android_column_names()
    x = 0
    for column in columns:
        worksheetVideoMax.write(x + AndGridStartRow, 0, column, cellFormat)
        x += 1

    #Writing the actual data in the workbook
    row = AndGridStartRow
    totalRows = 0

    android_pab = -999
    android_renewed = -999
    android_retained = -999
    android_churn = -999

    for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, \
        download_and_play_mins, dau_with_video_views_video_stream, dau_with_video_views_video_play, rn in rows:
        worksheetVideoMax.write(row, rn, date, date_format)
        worksheetVideoMax.write(row + 1, rn, uniques_shown_offer_popup, numFormat)
        worksheetVideoMax.write(row + 2, rn, new_user_activations, numFormat)
        worksheetVideoMax.write(row + 3, rn, daily_repeat_users, numFormat)
        worksheetVideoMax.write(row + 4, rn, video_stream_mins, numFormat)
        worksheetVideoMax.write(row + 5, rn, download_and_play_mins, numFormat)
        worksheetVideoMax.write(row + 6, rn, dau_with_video_views_video_stream, numFormat)
        worksheetVideoMax.write(row + 7, rn, dau_with_video_views_video_play, numFormat)
        worksheetVideoMax.write(row + 8, rn, android_pab, numFormat)
        worksheetVideoMax.write(row + 9, rn, android_renewed, numFormat)
        worksheetVideoMax.write(row + 10, rn, android_retained, numFormat)
        worksheetVideoMax.write(row + 11, rn, android_churn, numFormat)

        if totalRows < rn:
            totalRows = rn

    print 'Android Total Rows: ' + str(totalRows)
    worksheetAndroidAppUsersChart = workbook.add_worksheet('AndroidAppUsersChart')

    columns = get_android_column_names()
    x = 0
    for column in columns:
        worksheetAndroidAppUsersChart.write(0, x, column, cellFormat)
        x = x + 1

    row = 1
    col = 0

    for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, download_and_play_mins, dau_with_video_views_video_stream, dau_with_video_views_video_play, rn in rows:
        worksheetAndroidAppUsersChart.write(row, col, date, date_format)
        worksheetAndroidAppUsersChart.write(row, col + 1, uniques_shown_offer_popup, numFormat)
        worksheetAndroidAppUsersChart.write(row, col + 2, new_user_activations, numFormat)
        worksheetAndroidAppUsersChart.write(row, col + 3, daily_repeat_users, numFormat)
        worksheetAndroidAppUsersChart.write(row, col + 4, video_stream_mins, numFormat)
        worksheetAndroidAppUsersChart.write(row, col + 5, download_and_play_mins, numFormat)
        worksheetAndroidAppUsersChart.write(row, col + 6, dau_with_video_views_video_stream, numFormat)
        worksheetAndroidAppUsersChart.write(row, col + 7, dau_with_video_views_video_play, numFormat)

        row = row + 1

    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=AndroidAppUsersChart!$B$2:$B$' + str(totalRows),
        #'values': '=VideoMax!$A$' + str((AndGridStartRow+1)) + ':$' + currMetricColumn + '$' + str(AndGridStartRow+4),
        'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
        #'categories': '=VideoMax!$A$' + str((AndGridStartRow+2)) + ':$A$' + str(AndGridStartRow+4),
        'marker': {'type': 'square', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndroidAppUsersChart!$B$1',
        'line':   {'color': '#ed7d31'}
    })

    chart.add_series({
        'values': '=AndroidAppUsersChart!$C$2:$C$' + str(totalRows),
        'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'square', 'border': {'color': '#ffc000'}, 'fill':   {'color': '#ffc000'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndroidAppUsersChart!$C$1',
        'line':   {'color': '#ffc000'}
    })

    chart.add_series({
        'values': '=AndroidAppUsersChart!$D$2:$D$' + str(totalRows),
        'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'square', 'border': {'color': '#70ad47'}, 'fill':   {'color': '#70ad47'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndroidAppUsersChart!$D$1',
        'line':   {'color': '#70ad47'}
    })

    chart.set_title({'name': 'Android App Users', 'font_color': 'Grey'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(AndChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    query = get_query('ios', start_date)

    write_final_query_to_file('ios')

    rows = get_rows(query)

    worksheetVideoMax.write(iOSHeaderRow, 0, 'iOS', gridHeaderFormat)

    columns = get_ios_column_names()
    x = 0
    for column in columns:
        worksheetVideoMax.write(x + iOSGridStartRow, 0, column, cellFormat)
        x = x + 1

    ios_pab = -999
    ios_renewed = -999
    ios_retained = -999
    ios_churn = -999
    ios_mau = -999
    ios_new_act = -999
    ios_minutes = -999
    ios_mau_vv = -999

    row = iOSGridStartRow
    totalRows = 0

    for date, uniques_shown_offer_popup, new_user_activations, rn in rows:
        worksheetVideoMax.write(row, rn, date, date_format)
        worksheetVideoMax.write(row + 1, rn, uniques_shown_offer_popup, numFormat)
        worksheetVideoMax.write(row + 2, rn, new_user_activations, numFormat)
        worksheetVideoMax.write(row + 3, rn, ios_pab, numFormat)
        worksheetVideoMax.write(row + 4, rn, ios_renewed, numFormat)
        worksheetVideoMax.write(row + 5, rn, ios_retained, numFormat)
        worksheetVideoMax.write(row + 6, rn, ios_churn, numFormat)
        worksheetVideoMax.write(row + 7, rn, ios_mau, numFormat)
        worksheetVideoMax.write(row + 8, rn, ios_new_act, numFormat)
        worksheetVideoMax.write(row + 9, rn, ios_minutes, numFormat)
        worksheetVideoMax.write(row + 10, rn, ios_mau_vv, numFormat)


        if totalRows < rn: totalRows = rn

    print 'iOS Total Rows: ' + str(totalRows)
    worksheetiOSAppUsersChart = workbook.add_worksheet('iOSAppUsersChart')

    columns = get_ios_column_names()
    x = 0
    for column in columns:
        worksheetiOSAppUsersChart.write(0, x, column, cellFormat)
        x = x + 1

    row = 1
    col = 0

    for date, uniques_shown_offer_popup, new_user_activations, rn in rows:
        worksheetiOSAppUsersChart.write(row, col, date, date_format)
        worksheetiOSAppUsersChart.write(row, col + 1, uniques_shown_offer_popup, numFormat)
        worksheetiOSAppUsersChart.write(row, col + 2, new_user_activations, numFormat)

        row = row + 1

    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=iOSAppUsersChart!$B$2:$B$' + str(totalRows),
        'categories': '=iOSAppUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#a5a5a5'}, 'fill':   {'color': '#a5a5a5'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=iOSAppUsersChart!$B$1',
        'line':   {'color': '#a5a5a5'}
    })

    chart.add_series({
        'values': '=iOSAppUsersChart!$C$2:$C$' + str(totalRows),
        'categories': '=iOSAppUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=iOSAppUsersChart!$C$1',
        'line':   {'color': '#ed7d31'}
    })

    chart.set_title({'name': 'iOS App Users'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(iOSChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    #WAP code starts here
    #Replacing date parameter with current month start date as the reports are MTD
    query = get_query('wap', start_date)

    #Writing the executed query to Telkomsel_wap_ExecutedQuery.txt file in the same folder as the python script
    write_final_query_to_file('wap')

    rows = get_rows(query)

    worksheetVideoMax.write(WapHeaderRow, 0, 'WAP', gridHeaderFormat)

    #Getting the column names (headers) and writing it in the workbook
    columns = get_wap_column_names()
    x = 0
    for column in columns:
        worksheetVideoMax.write(x + WapGridStartRow, 0, column, cellFormat)
        x = x + 1

    #Writing the actual data in the workbook
    row = WapGridStartRow
    totalRows = 0

    wap_pab = -999
    wap_renewed = -999
    wap_retained = -999
    wap_churn = -999

    for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, dau_with_video_views, rn in rows:
        worksheetVideoMax.write(row, rn, date, date_format)
        worksheetVideoMax.write(row + 1, rn, uniques_shown_offer_popup, numFormat)
        worksheetVideoMax.write(row + 2, rn, new_user_activations, numFormat)
        worksheetVideoMax.write(row + 3, rn, daily_repeat_users, numFormat)
        worksheetVideoMax.write(row + 4, rn, video_stream_mins, numFormat)
        worksheetVideoMax.write(row + 5, rn, dau_with_video_views, numFormat)
        worksheetVideoMax.write(row + 6, rn, wap_pab, numFormat)
        worksheetVideoMax.write(row + 7, rn, wap_renewed, numFormat)
        worksheetVideoMax.write(row + 8, rn, wap_retained, numFormat)
        worksheetVideoMax.write(row + 9, rn, wap_churn, numFormat)

        if totalRows < rn: totalRows = rn

    print 'WAP Total Rows: ' + str(totalRows)
    worksheetWAPUsersChart = workbook.add_worksheet('WAPUsersChart')

    columns = get_wap_column_names()
    x = 0
    for column in columns:
        worksheetWAPUsersChart.write(0, x, column, cellFormat)
        x = x + 1

    row = 1
    col = 0

    for date, uniques_shown_offer_popup, new_user_activations, daily_repeat_users, video_stream_mins, dau_with_video_views, rn in rows:
        worksheetWAPUsersChart.write(row, col, date, date_format)
        worksheetWAPUsersChart.write(row, col + 1, uniques_shown_offer_popup, numFormat)
        worksheetWAPUsersChart.write(row, col + 2, new_user_activations, numFormat)
        worksheetWAPUsersChart.write(row, col + 3, daily_repeat_users, numFormat)
        worksheetWAPUsersChart.write(row, col + 4, video_stream_mins, numFormat)
        worksheetWAPUsersChart.write(row, col + 5, dau_with_video_views, numFormat)

        row = row + 1

    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=WAPUsersChart!$B$2:$B$' + str(totalRows),
        'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'square', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=WAPUsersChart!$B$1',
        'line':   {'color': '#ed7d31'}
    })

    chart.add_series({
        'values': '=WAPUsersChart!$C$2:$C$' + str(totalRows),
        'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'square', 'border': {'color': '#ffc000'}, 'fill':   {'color': '#ffc000'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=WAPUsersChart!$C$1',
        'line':   {'color': '#ffc000'}
    })

    chart.add_series({
        'values': '=WAPUsersChart!$D$2:$D$' + str(totalRows),
        'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'square', 'border': {'color': '#70ad47'}, 'fill':   {'color': '#70ad47'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=WAPUsersChart!$D$1',
        'line':   {'color': '#70ad47'}
    })

    chart.set_title({'name': 'Mobile Browser Users'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(WapChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    #Android Consumption Chart
    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=AndroidAppUsersChart!$E$2:$E$' + str(totalRows),
        'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'square', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndroidAppUsersChart!$E$1',
        'line':   {'color': '#ed7d31'},
        'trendline': {'type': 'linear', 'line': {
                                                'color': '#ed7d31',
                                                'width': 1,
                                                'dash_type': 'long_dash'
                                                }
                    }
    })

    chart.add_series({
        'values': '=AndroidAppUsersChart!$F$2:$F$' + str(totalRows),
        'categories': '=AndroidAppUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#ffc000'}, 'fill':   {'color': '#ffc000'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndroidAppUsersChart!$F$1',
        'line':   {'color': '#ffc000'}
    })

    chart.set_title({'name': 'Android Consumption (Mins)'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(AndConsumptionChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    #WAP Consumption Chart
    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=WAPUsersChart!$E$2:$E$' + str(totalRows),
        'categories': '=WAPUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=WAPUsersChart!$E$1',
        'line':   {'color': '#ed7d31'}
    })

    chart.set_title({'name': 'Mobile Browser Consumption (Mins)'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(WapConsumptionChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    #Android Video Consumption on Mobile vs WiFi code starts here
    #Replacing date parameter with current month start date as the reports are MTD
    query = get_query('android_mvw', start_date)

    #Writing the executed query to Telkomsel_Android_MvW_ExecutedQuery.txt file in the same folder as the python script
    write_final_query_to_file('android_mvw')
    rows = get_rows(query)

    worksheetVideoMax.write(AndMvWHeaderRow, 0, 'Mobile Network Vs Wifi', gridHeaderFormat)

    #Getting the column names (headers) and writing it in the workbook
    columns = get_android_mvw_columns()
    x = 0
    for column in columns:
        worksheetVideoMax.write(x + AndMvWGridStartRow, 0, column, cellFormat)
        x = x + 1

    #Writing the actual data in the workbook
    row = AndMvWGridStartRow
    totalRows = 0

    for date, mobile_video_mins, wifi_video_mins, total_video_mins, mobile_video_percentage, wifi_video_percentage, rn in rows:
        worksheetVideoMax.write(row, rn, date, date_format)
        worksheetVideoMax.write(row + 1, rn, mobile_video_mins, numFormat)
        worksheetVideoMax.write(row + 2, rn, wifi_video_mins, numFormat)
        worksheetVideoMax.write(row + 3, rn, total_video_mins, numFormat)
        worksheetVideoMax.write(row + 4, rn, mobile_video_percentage, percNumFormat)
        worksheetVideoMax.write(row + 5, rn, wifi_video_percentage, percNumFormat)

        if totalRows < rn: totalRows = rn

    print 'Android Mobile vs WiFi Consumption Total Rows: ' + str(totalRows)
    worksheetAndMvWUsersChart = workbook.add_worksheet('AndMvWUsersChart')

    columns = get_android_mvw_columns()
    x = 0
    for column in columns:
        worksheetAndMvWUsersChart.write(0, x, column, cellFormat)
        x = x + 1

    row = 1
    col = 0

    for date, mobile_video_mins, wifi_video_mins, total_video_mins, mobile_video_percentage, wifi_video_percentage, rn in rows:
        worksheetAndMvWUsersChart.write(row, col, date, date_format)
        worksheetAndMvWUsersChart.write(row, col + 1, mobile_video_mins, numFormat)
        worksheetAndMvWUsersChart.write(row, col + 2, wifi_video_mins, numFormat)
        worksheetAndMvWUsersChart.write(row, col + 3, total_video_mins, numFormat)
        worksheetAndMvWUsersChart.write(row, col + 4, mobile_video_percentage, percNumFormat)
        worksheetAndMvWUsersChart.write(row, col + 5, wifi_video_percentage, percNumFormat)

        row = row + 1

    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=AndMvWUsersChart!$B$2:$B$' + str(totalRows),
        'categories': '=AndMvWUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#a3c7e7'}, 'fill':   {'color': '#a3c7e7'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndMvWUsersChart!$B$1',
        'line':   {'color': '#a3c7e7'}
    })

    chart.add_series({
        'values': '=AndMvWUsersChart!$C$2:$C$' + str(totalRows),
        'categories': '=AndMvWUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#ed7d31'}, 'fill':   {'color': '#ed7d31'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=AndMvWUsersChart!$C$1',
        'line':   {'color': '#ed7d31'}
    })

    chart.set_title({'name': 'Mobile Network Vs WiFi'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(AndMvWChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    #WiFi code starts here
    #Replacing date parameter with current month start date as the reports are MTD
    query = get_query('wifi', start_date)

    #Writing the executed query to Telkomsel_WiFi_MvW_ExecutedQuery.txt file in the same folder as the python script
    write_final_query_to_file('wifi')

    rows = get_rows(query)

    worksheetVideoMax.write(WiFiHeaderRow, 0, 'Wifi - IP Based', gridHeaderFormat)

    #Getting the column names (headers) and writing it in the workbook
    columns = get_wifi_columns()
    x = 0
    for column in columns:
        worksheetVideoMax.write(x + WiFiGridStartRow, 0, column, cellFormat)
        x = x + 1

    #Writing the actual data in the workbook
    row = WiFiGridStartRow
    totalRows = 0

    for date, wifi_ip_percentage, rn in rows:
        worksheetVideoMax.write(row, rn, date, date_format)
        worksheetVideoMax.write(row + 1, rn, wifi_ip_percentage, percNumFormat)

        if totalRows < rn: totalRows = rn

    print 'WiFi Total Rows: ' + str(totalRows)
    worksheetWiFiUsersChart = workbook.add_worksheet('WiFiUsersChart')

    columns = get_wifi_columns()
    x = 0
    for column in columns:
      worksheetWiFiUsersChart.write(0, x, column, cellFormat)
      x = x + 1

    row = 1
    col = 0

    for date, wifi_ip_percentage, rn in rows:
        worksheetWiFiUsersChart.write(row, col, date, date_format)
        worksheetWiFiUsersChart.write(row, col + 1, wifi_ip_percentage, percNumFormat)

        row = row + 1

    #Adding the chart
    chart = workbook.add_chart({'type': 'line'})

    # Add series to the chart
    chart.add_series({
        'values': '=WiFiUsersChart!$B$2:$B$' + str(totalRows),
        'categories': '=WiFiUsersChart!$A$2:$A$' + str(totalRows),
        'marker': {'type': 'circle', 'border': {'color': '#a3c7e7'}, 'fill':   {'color': '#a3c7e7'},},
        'data_labels': {'value': True, 'position': 'above'},
        'name': '=WiFiUsersChart!$B$1',
        'line':   {'color': '#a3c7e7'}
    })

    chart.set_title({'name': 'WiFi - IP Based'})

    chart.set_plotarea({
        'layout': {
            'x':      0.05,
            'y':      0.2,
            'width':  0.9,
            'height': 0.6,
        }
    })

    chart.set_legend({'position': 'bottom'})
    chart.set_size({'width': chartWidth, 'height': 276})

    chartPosition = 'A' + str(WiFiChartStartRow)
    worksheetVideoMax.insert_chart(chartPosition, chart, {'x_offset': 10, 'y_offset': 5})

    #Adding a sheet for Terms and Conditions and adding contents to it
    worksheetTnC = workbook.add_worksheet('Terms & Conditions')

    worksheetTnC.hide_gridlines(2)
    worksheetTnC.set_column('A:A', 1)
    worksheetTnC.set_column('B:B', 0.5)

    worksheetTnC.insert_image('A1', 'tncImageViu.png', {'x_scale': 1, 'y_scale': 1})

    worksheetTnC.write(3, 2, 'Terms & Conditions:', tncHeaderFormat)
    worksheetTnC.write(4, 0, '1', tncNumFormat)
    worksheetTnC.write(4, 2, 'This report is for usage of intended partnership recipients only & for restricted circulation only.', tncFormat)
    worksheetTnC.write(5, 0, '2', tncNumFormat)
    worksheetTnC.write(5, 2, 'Modification of this report is not permitted.', tncFormat)
    worksheetTnC.write(6, 0, '3', tncNumFormat)
    worksheetTnC.write(6, 2, 'All data in this report is highly confidential & to be treated in accordance with the terms of the signed agreement between the Partner and Vuclip or its authorised representatives.', tncFormat)
    worksheetTnC.write(7, 0, '4', tncNumFormat)
    worksheetTnC.write(7, 2, 'This report is the property of Vuclip Singapore Pte Ltd. & all trademarks are the sole property of respective owners.', tncFormat)
    worksheetTnC.write(8, 0, '5', tncNumFormat)
    worksheetTnC.write(8, 2, 'Editing, distortion, omission, conversion and/or reeinginering of this report is not permitted.', tncFormat)
    worksheetTnC.write(9, 0, '6', tncNumFormat)
    worksheetTnC.write(9, 2, 'Not permitted to be used for public display, or as a reference report for further analytics.', tncFormat)
    worksheetTnC.write(10, 0, '7', tncNumFormat)
    worksheetTnC.write(10, 2, 'Terms and conditions may be modified from time to time & for the latest terms please touch base with your Partnership Manager.', tncFormat)

    worksheetVideoMax.freeze_panes(1, 0)
    worksheetVideoMax.hide_gridlines(2)
    worksheetAndroidAppUsersChart.hide()
    worksheetiOSAppUsersChart.hide()
    worksheetWAPUsersChart.hide()
    worksheetAndMvWUsersChart.hide()
    worksheetWiFiUsersChart.hide()
    workbook.close()

except ValueError :
    print "An error occurred during the processing. Closing the workbook"
    workbook.close()

time.sleep(3)

print("Sending email to")

recipients = ['abhinav.vajpayee@vuclip.com']
#recipients = ['meri.djajakustio@vuclip.com','varun@vuclip.com','utsav@vuclip.com','tushar.harduley@vuclip.com','abhinav.vajpayee@vuclip.com','sidhant.das@vuclip.com','rahul.badki@vuclip.com']
emaillist = [elem.strip().split(',') for elem in recipients]
msg = MIMEMultipart()
msg['Subject'] = str('Telkomsel Daily Report - ' + fileNameDate)
msg['From'] = 'analytics@vuclip.com'
msg['Reply-to'] = 'analytics@vuclip.com'
msg['To'] = ", ".join(recipients)

msg.preamble = 'Multipart massage.\n'

part = MIMEText("Hi, \r\nPlease find attached the file containing Telkomsel Daily Report.")
msg.attach(part)

part = MIMEApplication(open(str(workbookName),"rb").read())
part.add_header('Content-Disposition', 'attachment', filename=str(workbookName))

msg.attach(part)

print  ", ".join(recipients)

p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
#p.communicate(msg.as_string())

print("Email sent successfully")
print("Ending the script")