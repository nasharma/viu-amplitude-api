def write_all_log_types_to_google_storage(self, local_data_file, log_file_name, event_type):
    logging.info('Starting GCS upload...')
    credentials, project = auth.default()
    storage_client = storage.Client(self.google_bigquery_project_name, credentials)
    gcs_bucket = storage_client.get_bucket(self.google_storage_bucket.bucket_name)
    gcs_object = gcs_bucket.blob('{}/{}_{}'.format(
        self.start_date, log_file_name, event_type))
    gcs_object.upload_from_filename(local_data_file, content_type='application/text')
    gcs_file_name = "gs://{}/{}".format(gcs_object.bucket.name, gcs_object.name)
    logging.info('GCS file {} uploaded!'.format(gcs_file_name))
    logging.debug('Removing local file...')
    # os.remove(local_data_file)
    logging.debug('Local file {} removed!'.format(local_data_file))
    return gcs_file_name