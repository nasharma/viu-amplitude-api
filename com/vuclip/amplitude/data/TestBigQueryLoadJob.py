import logging

import time
from datetime import date

from google.cloud import bigquery
import uuid

from google import auth

def load_bigquery ():
    logging.info('Starting BigQuery load...')
    dataset_name = 'viu_amplitude_test'
    storage_file_name = "gs://{}/{}".format('viu-amplitude-test', 'sample')
    # table name should be appended with $YYYYMMDD in order to re-state a day's partition.
    table_name = 'test_load'
    logging.debug('parameters for BQ load ==>' + dataset_name + '|' + table_name + '|' + storage_file_name)
    credentials, project = auth.default()
    bigquery_client = bigquery.Client("insights-sandbox-153010", credentials)
    dataset = bigquery_client.dataset(dataset_name)
    table = dataset.table(table_name)

    job_name = 'amplitude_bq_load_' + str(uuid.uuid4())
    table.reload()

    job = table.upload_from_file(open('d:\\sample', 'rb'), 'NEWLINE_DELIMITED_JSON',
                         write_disposition='WRITE_TRUNCATE')
    #job = bigquery_client.load_table_from_storage(job_name, table, storage_file_name)
    #job.skip_leading_rows = 1
    #job.write_disposition = 'WRITE_TRUNCATE'  # overwrite existing partition
    #job.begin()
    print(job.state)
    wait_for_job(job)
    logging.info('Loaded {} rows in {}:{}.'.format(job.output_rows, dataset_name, table_name))


def wait_for_job(job):
    while True:
        job.reload()
        print(job.state)
        if job.state == 'DONE':
            if job.error_result:
                raise RuntimeError(job.errors)
            return
    time.sleep(1)

#load_bigquery()

def query_bigquery():
    query = ''
    f = open("Telkomsel_Android_ExecutedQuery.txt", "w+")
    f.write(str(query))
    f.close()

def query_named_params(corpus, min_word_count):
    credentials, project = auth.default()
    client = bigquery.Client('vuclipdataflow-1301', credentials)
    query = """
        SELECT word, word_count
        FROM `bigquery-public-data.samples.shakespeare`
        WHERE corpus = @corpus
        AND word_count >= @min_word_count
        ORDER BY word_count DESC;
        """
    query_job = client.run_async_query(
        str(uuid.uuid4()),
        query,
        query_parameters=(
            bigquery.ScalarQueryParameter('corpus', 'STRING', corpus),
            bigquery.ScalarQueryParameter(
                'min_word_count', 'INT64', min_word_count)))
    query_job.use_legacy_sql = False

    # Start the query and wait for the job to complete.
    query_job.begin()
    wait_for_job(query_job)
    print_results(query_job.results())

def wait_for_job(job):
    while True:
        job.reload()  # Refreshes the state via a GET request.
        if job.state == 'DONE':
            if job.error_result:
                raise RuntimeError(job.errors)
            return
        time.sleep(1)

def print_results(query_results):
    """Print the query results by requesting a page at a time."""
    page_token = None

    while True:
        rows, total_rows, page_token = query_results.fetch_data(
            max_results=31,
            page_token=page_token)

        for row in rows:
            print(row)

        if not page_token:
            break

#query_named_params('sonnets',1)
start_date = date.today()
print start_date.strftime('%Y-%m-%d')