'''
Created on 08-Mar-2017

@author: Koushik Kulkarni
'''

import argparse
from datetime import datetime, time, timedelta
import logging
import uuid
import readconfig
import smtplib
import os
from boto.s3.connection import S3Connection
from email.mime.text import MIMEText
from subprocess import Popen, PIPE
from google.cloud import bigquery
from google.cloud import storage


def send_mail (file_name, config, load_date, success_status, err):
    logging.debug('sending email...')
    SERVER = "localhost"
    FROM = "notifications@viu-etl-1.vuclipdataflow-1301.com"

    if success_status == 'Success':
        SUBJECT = "AppsFlyer {} Load Successfully Completed For Date: {}".format (file_name, load_date)
        TO = config.get('success_email_list').split(',')
        TEXT = ''
    else:
        SUBJECT = "AppsFlyer {} Load Failed For Date: {}".format (file_name, load_date)
        TO = config.get('failure_email_list').split(',')
        TEXT = 'Exception : {}'.format(err)
    message = """\
From: %s
To: %s
Subject: %s

%s
""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
    server = smtplib.SMTP(SERVER)
    server.set_debuglevel(3)
    server.sendmail(FROM, TO, message)
    server.quit()
    logging.debug('email sent!')

def wait_for_job(job):
    while True:
        job.reload()
        if job.state == 'DONE':
            if job.error_result:
                raise RuntimeError(job.errors)
            return
    time.sleep(1)

def download_from_s3 (config, source_prefix, report_date):
    logging.debug('Starting S3 extraction...')
    access_key = config.get('s3_access_key')
    secret_key = config.get('s3_secret_key')
    conn = S3Connection(access_key, secret_key)
    s3_bucket = conn.get_bucket(config.get('s3_bucket'), validate=False)
    s3_key = s3_bucket.get_key('{}/{}-{}/{}'.format(
        config.get('s3_folder'), source_prefix, report_date, config.get('s3_file_name')))
    local_file_name = '{}/{}_{}_{}{}'.format(
        config.get('local_data_dir'), source_prefix, report_date, datetime.now().strftime("%Y%m%d%H%M%S"), config.get('local_file_suffix'))
    logging.debug('Local file name: {}'.format(local_file_name))
    s3_key.get_contents_to_filename(local_file_name)
    logging.info('Local file {} saved'.format(local_file_name))
    logging.info('File download complete for {} data for reporting date {}'.format(source_prefix, report_date))
    return local_file_name

def upload_to_gcs (config, source_prefix, report_date, local_file_name):
    logging.info('Starting GCS upload...')
    storage_client = storage.Client()
    gcs_bucket = storage_client.get_bucket(config.get('gcs_bucket_name'))
    gcs_object = gcs_bucket.blob('{}/{}_{}_{}{}'.format(
        report_date, source_prefix, report_date, datetime.now().strftime("%Y%m%d%H%M%S"), config.get('gcs_file_suffix')))
    gcs_object.upload_from_filename(local_file_name, content_type = 'application/gzip')
    gcs_file_name = "gs://{}/{}".format(gcs_object.bucket.name, gcs_object.name)
    logging.info('GCS file {} uploaded!'.format(gcs_file_name))
    logging.debug('Removing local file...')
    os.remove(local_file_name)
    logging.debug('Local file {} removed!'.format(local_file_name))
    return gcs_file_name

def load_bigquery (config, source_prefix, report_date, storage_file_name, skip_header):
    logging.info('Starting BigQuery load...')
    dataset_name = config.get('bigquery_dataset_name')
    # table name should be appended with $YYYYMMDD in order to re-state a day's partition.
    table_name = config.get(source_prefix + '_table_name') + '$' + datetime.strptime(report_date, "%Y-%m-%d").strftime('%Y%m%d')
    logging.debug('parameters for BQ load ==>' + dataset_name + '|' + table_name + '|' + storage_file_name)
    bigquery_client = bigquery.Client()
    dataset = bigquery_client.dataset(dataset_name)
    table = dataset.table(table_name)

    job_name = 'appsflyer_bq_load_' + str(uuid.uuid4())
    job = bigquery_client.load_table_from_storage(job_name, table, storage_file_name)
    if skip_header:
        job.skip_leading_rows = 1
    job.write_disposition = 'WRITE_TRUNCATE'  # overwrite existing partition
    job.begin()

    wait_for_job(job)
    logging.info('Loaded {} rows in {}:{}.'.format(job.output_rows, dataset_name, table_name))
    logging.info('BigQuery load complete for {} data for reporting date {}'.format(source_prefix, report_date))


def extract_and_load_data (config, source_prefix, report_date):
    logging.debug("Report date ==> {}".format(report_date))
    # Extract
    try :
        local_file_name = download_from_s3 (config, source_prefix, report_date)
    except Exception as ex:
        logging.error("Error occurred during data download for {} data for reporting date {}! Exception: {}".format(source_prefix, report_date, ex))
        raise ex

    # Upload to GCS
    try:
        gcp_file_name = upload_to_gcs(config, source_prefix, report_date, local_file_name)
    except Exception as ex:
        logging.error("Error occurred during GCS upload  for {} data for reporting date {}! Exception: {}".format(source_prefix, report_date, ex))
        raise ex

    # load in big query
    skip_header = True
    try:
        load_bigquery (config, source_prefix, report_date, gcp_file_name, skip_header)
    except Exception as ex:
        logging.error("Error occurred while uploading {} data for reporting date {} to BigQuery! Exception: {}".format(source_prefix, report_date, ex))
        raise ex

def run(config, report_date):
    logging.info("Report date => {}".format(report_date))

    e = None
    if config.get('load_installs_data') == 'Y':
        try:
            extract_and_load_data(config, config.get('installs_table_prefix'), report_date)
            send_mail('Installs', config, report_date, 'Success', None)
            logging.debug('AppsFlyer Installs data extracted and loaded successfully!')
        except Exception as err:
            logging.error("Error occurred during Installs data extract/load! {}".format(err))
            send_mail('Installs', config, report_date, 'Error', err)
            e = err

    if config.get('load_inapp_data') == 'Y':
        try:
            extract_and_load_data(config, config.get('inapp_table_prefix'), report_date)
            send_mail('Events', config, report_date, 'Success', None)
            logging.debug('AppsFlyer Events data extracted and loaded successfully!')
        except Exception as err:
            logging.error("Error occurred during Events data extract/load! {}".format(err))
            send_mail('Events', config, report_date, 'Error', err)
            e = err
    if e is not None:
        raise e


if __name__ == '__main__':
    print ('Starting Appsflyer load...')
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('config_file', help='Fully qualified config file name')
    parser.add_argument('environment', help='Environment to run - must be one of dev, sit or prod')
    parser.add_argument('report_date', nargs='?', default='default', help='Optional reporting date - if not provided, this will be taken from config. ')
    args = parser.parse_args()

    config = readconfig.parse_config_file(args.environment, args.config_file)

    report_date = args.report_date
    if report_date == 'default' :
        report_date = config.get('report_date')
    if report_date == '' :
        report_date = (datetime.today() - timedelta(days=1)).strftime('%Y-%m-%d')

    logfile = '{}/{}_{}.log'.format(config.get('logdir'), 'appsflyerload', report_date)
    print "Starting load for {} with config {} in environment {}. See log file {} for details.".format(report_date, args.config_file, args.environment, logfile)

    logging.basicConfig(level=logging.INFO, filename=logfile,
                        filemode="a+", format="%(asctime)-15s %(levelname)-8s %(message)s")

    run(config,report_date)
