# syntax : python truncate_partition.py <partition_date> <table list file name>
# where:
# partition_date is the date for which the partition in the target table needs to be emptied
# table list file name is list of dataset_name.table_name, one on each line

import argparse
import logging
import uuid
import time 
from google.cloud import bigquery

def wait_for_job(job):
    while True:
        job.reload()  # Refreshes the state via a GET request.
        if job.state == 'DONE':
            if job.error_result:
                raise RuntimeError(job.errors)
            return
        logging.debug("job still running... sleeping for 2 seconds")
        time.sleep(2)

      
logfile = './truncate.log'

logging.basicConfig(level=logging.DEBUG, filename=logfile, filemode = "a+", format = "%(asctime)-15s %(levelname)-8s %(message)s")

parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('process_date', help='date for which the partition needs to be deleted in YYYYMMDD format')
parser.add_argument('table_list', help='list of tables for which the partition needs to be deleted')
args = parser.parse_args()

bigquery_client = bigquery.Client()
tmplt_query = "select * from {} where _partitiontime = '3499-12-31';" 
with open(args.table_list) as f:
   for line in f:
      dataset_name, table_name = line.split('.')
      logging.info('truncating data for table {} for date {}...'.format(line, args.process_date ))
      query = tmplt_query.format(line)
      job_name = 'truncate_job_{}_{}_{}'.format(line.replace('.','_'), args.process_date, str(uuid.uuid4()))
      job = bigquery_client.run_async_query(job_name, query)
      job.destination = bigquery_client.dataset(dataset_name).table(table_name + '${}'.format(args.process_date))
      job.use_legacy_sql = False
      job.write_disposition = 'WRITE_TRUNCATE'
      job.begin()
      wait_for_job(job)
      logging.info('truncated data for table {} for date {}!'.format(line, args.process_date ))
