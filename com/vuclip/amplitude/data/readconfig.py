import ConfigParser

def parse_config_file(sec, configfile):
    configdict = {}
    config = ConfigParser.RawConfigParser()
    config.read(configfile)
    options = config.options(sec)
    for option in options:
        try:
            configdict[option] = config.get(sec, option)
        except:
            print 'Exception!! could not find value for {}'.format(option)
            configdict[option] = None
    return configdict


