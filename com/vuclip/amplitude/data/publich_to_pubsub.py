import getopt

import sys

from google import auth
from google.cloud import pubsub

file_name = ''
topic_name = ''
project_name = 'mm-insights-prod'

def publish_message(topic_name, data):
    credentials, project = auth.default()
    pubsub_client = pubsub.Client(project_name, credentials)
    topic = pubsub_client.topic(topic_name)
    # Data must be a bytestring
    data = data.encode('utf-8')
    message_id = topic.publish(data)
    print('Message {} published.'.format(message_id))


def main(argv):
    global file_name, topic_name
    try:
        opts, args = getopt.getopt(argv, "h:t:f:",
                                   ["topic_name","file_name"])
    except getopt.GetoptError:
        print 'publish_to_pubsub.py -t <topic_name> -f <filename>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'publish_to_pubsub.py -t <topic_name> -f <filename>'
            sys.exit()
        elif opt in ("-t", "--topic_name"):
            topic_name = arg
        elif opt in ("-f", "--file_name"):
            file_name = arg



if __name__ == "__main__":
    main(sys.argv[1:])
print("Topic: {} FileName: {}".format(topic_name, file_name))

with open(file_name, 'r') as data_file:
    for line in data_file:
        publish_message(topic_name, line)


